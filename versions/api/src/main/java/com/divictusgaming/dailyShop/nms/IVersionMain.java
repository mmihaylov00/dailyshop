package com.divictusgaming.dailyShop.nms;

import org.bukkit.command.CommandMap;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public interface IVersionMain {

    CommandMap getCommandMap(JavaPlugin plugin);

    ItemStack setNmsTags(ItemStack itemStack, String command);

    ItemTag getTags(ItemStack itemStack);

    class ItemTag{
        private final String command;
        private final Boolean isFromMenu;

        public ItemTag(String command, Boolean isFromMenu) {
            this.command = command;
            this.isFromMenu = isFromMenu;
        }

        public String getCommand() {
            return command;
        }

        public Boolean getFromMenu() {
            return isFromMenu;
        }
    }

    enum ItemTagName{
        COMMAND("dailyshop-command"),
        ITEM("dailyshop-item");

        private final String tag;

        ItemTagName(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }
    }
}
