package com.divictusgaming.dailyShop.nms.v1_18_R1;

import com.divictusgaming.dailyShop.nms.IVersionMain;
import net.minecraft.nbt.NBTTagCompound;
import org.bukkit.command.CommandMap;
import org.bukkit.craftbukkit.v1_18_R1.CraftServer;
import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class VersionMain implements IVersionMain {

    public CommandMap getCommandMap(JavaPlugin plugin) {
        return ((CraftServer) plugin.getServer()).getCommandMap();
    }

    public ItemStack setNmsTags(ItemStack itemStack, String command) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound compound = (nmsItem.r()) ? nmsItem.s() : new NBTTagCompound();
        compound.a(ItemTagName.ITEM.getTag(), true);
        if (command != null)
            compound.a(ItemTagName.COMMAND.getTag(), command);

        nmsItem.c(compound);
        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    @Override
    public ItemTag getTags(ItemStack itemStack) {
        net.minecraft.world.item.ItemStack item = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound tag;
        if (item == null || !item.r() || (tag = item.s()) == null) return null;

        return new ItemTag(tag.l(ItemTagName.COMMAND.getTag()), tag.q(ItemTagName.ITEM.getTag()));
    }
}
