package com.divictusgaming.dailyShop.nms.v1_16_R1;

import com.divictusgaming.dailyShop.nms.IVersionMain;
import net.minecraft.server.v1_16_R1.NBTTagCompound;
import org.bukkit.command.CommandMap;
import org.bukkit.craftbukkit.v1_16_R1.CraftServer;
import org.bukkit.craftbukkit.v1_16_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class VersionMain implements IVersionMain {

    public CommandMap getCommandMap(JavaPlugin plugin) {
        return ((CraftServer) plugin.getServer()).getCommandMap();
    }

    public ItemStack setNmsTags(ItemStack itemStack, String command) {
        net.minecraft.server.v1_16_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound compound = (nmsItem.hasTag()) ? nmsItem.getTag() : new NBTTagCompound();
        compound.setBoolean(ItemTagName.ITEM.getTag(), true);
        if (command != null)
            compound.setString(ItemTagName.COMMAND.getTag(), command);

        nmsItem.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsItem);
    }

    @Override
    public ItemTag getTags(ItemStack itemStack) {
        net.minecraft.server.v1_16_R1.ItemStack item = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound tag;
        if (item == null || !item.hasTag() || (tag = item.getTag()) == null) return null;

        return new ItemTag(tag.getString(ItemTagName.COMMAND.getTag()), tag.getBoolean(ItemTagName.ITEM.getTag()));
    }
}
