package com.divictusgaming.dailyShop.exception;

public class EmptyConfigurationException extends Exception {
    private final String name;

    public EmptyConfigurationException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
