package com.divictusgaming.dailyShop.exception;

public class ConfigurationNotSetException extends Exception {
    private final String name;

    public ConfigurationNotSetException(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
