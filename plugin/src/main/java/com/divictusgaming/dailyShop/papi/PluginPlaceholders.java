package com.divictusgaming.dailyShop.papi;

import com.divictusgaming.dailyShop.util.PlaceholderReplacer;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Locale;

public class PluginPlaceholders extends me.clip.placeholderapi.expansion.PlaceholderExpansion {

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "dailyshop";
    }

    @Override
    public String getAuthor() {
        return "ZeaL_BGN";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        try {
            return PlaceholderReplacer.Placeholder.valueOf(identifier.toUpperCase(Locale.ROOT)).getValue(player);
        } catch (IllegalArgumentException ignored) {
        }
        return null;
    }

    private String getValue(OfflinePlayer player, String identifier) {
        return null;
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        return getValue(player, identifier);
    }
}
