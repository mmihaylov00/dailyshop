package com.divictusgaming.dailyShop.storage;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.IDatabase;
import com.divictusgaming.dailyShop.database.configuration.*;
import com.divictusgaming.dailyShop.gui.Menu;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class PlayerDataStorage {
    private final HashMap<UUID, PlayerConfiguration> configurations;

    public PlayerConfiguration getPlayerData(UUID uuid) {
        return configurations.get(uuid);
    }

    public PlayerDataStorage() {
        this.configurations = new HashMap<>();
    }

    public void loadPlayer(UUID uuid) {
        loadPlayer(uuid.toString());
    }

    public void loadPlayer(String uuid) {
        Main plugin = Main.getPlugin(Main.class);
        plugin.getDatabase()
                .loadData("uuid", uuid, PlayerConfiguration.class, new IDatabase.LoadDataCallback<PlayerConfiguration>() {
                    @Override
                    public void onQueryDone(PlayerConfiguration result, List<PlayerConfiguration> results) {
                        configurations.put(UUID.fromString(uuid), result);
                        generatePlayerItems(result);
                    }

                    @Override
                    public void onError(Exception e) {
                        PlayerConfiguration configuration = new PlayerConfiguration(uuid);
                        plugin.getDatabase().saveData("uuid", uuid, configuration,
                                (result, results) -> {
                                    configurations.put(UUID.fromString(uuid), result);
                                    generatePlayerItems(result);
                                });
                    }
                });
    }

    public void savePlayer(UUID uuid, boolean unload) {
        PlayerConfiguration player = unload ?
                configurations.remove(uuid) : configurations.get(uuid);
        if (player == null) return;
        savePlayer(player);
    }

    public void savePlayer(PlayerConfiguration player) {
        Main.getPlugin(Main.class).getDatabase().saveData("uuid", player.getUuid(), player);
    }

    public void generatePlayerItems(PlayerConfiguration player) {
        Main plugin = Main.getPlugin(Main.class);
        Player p = Bukkit.getPlayer(UUID.fromString(player.getUuid()));

        HashMap<String, Double> parsedPlaceholders = new HashMap<>();

        for (CategoryConfiguration category : plugin.getCategoryStorage().getCategories()) {
            List<Integer> slots = getFreeSlots(player, category);

            if (slots.isEmpty()) continue;

            HashMap<ShopItemConfiguration, Double> possibleItems = getShopItems(p, parsedPlaceholders, category);

            double sum = 0.0;
            for (ShopItemConfiguration configuration : possibleItems.keySet()) {
                double percentage = configuration.getPercentage();
                sum += percentage;
            }

            fillItems(player, slots, possibleItems, sum);
        }

        Menu lastOpen = plugin.getMenuStorage().getLastOpen(p.getUniqueId());
        if (lastOpen != null) lastOpen.refreshInventory(p);
    }

    private void fillItems(PlayerConfiguration player, List<Integer> slots, HashMap<ShopItemConfiguration, Double> possibleItems, double sum) {
        for (Integer slot : slots) {
            double search = sum * new Random().nextDouble();
            double current = sum;

            for (Map.Entry<ShopItemConfiguration, Double> entry : possibleItems.entrySet()) {
                current -= entry.getKey().getPercentage();
                if (current < search) {
                    player.addItem(new PlayerShopItemConfiguration(player.getId(), entry.getKey(), slot, entry.getValue()));
                    break;
                }
            }
        }
    }

    private List<Integer> getFreeSlots(PlayerConfiguration player, CategoryConfiguration category) {
        List<Integer> slots = new ArrayList<>();
        for (Integer slot : category.getItemSlots()) {
            boolean isOccupied = false;
            for (PlayerShopItemConfiguration c : player.getItems()) {
                if (c.getCategory().equals(category.getName()) && c.getSlot() == slot) {
                    isOccupied = true;
                    break;
                }
            }
            if (isOccupied) continue;

            slots.add(slot);
        }
        return slots;
    }


    public void removeItem(Player p, PlayerShopItemConfiguration item) {
        PlayerConfiguration data = getPlayerData(p.getUniqueId());
        if (data.removeItem(item))
            generatePlayerItems(data);
    }

    public void refreshAllItems(Player player) {
        PlayerConfiguration playerData = this.getPlayerData(player.getUniqueId());
        playerData.getItems().clear();
        generatePlayerItems(playerData);
    }

    public void refreshCategory(Player player, String category) {
        PlayerConfiguration playerData = this.getPlayerData(player.getUniqueId());
        playerData.getItems().removeIf(configuration -> configuration.getCategory().equals(category));
        generatePlayerItems(playerData);
    }

    public void refreshAllPlayers() {
        Main pl = Main.getPlugin(Main.class);
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers())
                    pl.getPlayerDataStorage().refreshAllItems(player);
            }
        }.runTaskAsynchronously(pl);
        pl.getDatabase().clearAllData(PlayerConfiguration.class);
    }

    public void saveAllPlayers() {
        Main pl = Main.getPlugin(Main.class);
        for (PlayerConfiguration player : configurations.values())
            pl.getDatabase().saveDataSync("uuid", player.getUuid(), player);
    }


    private HashMap<ShopItemConfiguration, Double> getShopItems(Player p, HashMap<String, Double> parsedPlaceholders, CategoryConfiguration category) {
        HashMap<ShopItemConfiguration, Double> possibleItems = new HashMap<>();
        for (ShopItemConfiguration shopItem : category.getShopItems()) {
            for (ConditionConfiguration condition : shopItem.getConditions()) {
                Double value;
                if ((value = parsedPlaceholders.get(condition.getPlaceholder())) == null) {
                    try {
                        value = Double.parseDouble(PlaceholderAPI.setPlaceholders(p, "%" + condition.getPlaceholder() + "%"));
                    } catch (NumberFormatException e) {
                        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "The " + condition.getPlaceholder() +
                                " placeholder from one of " + category.getName() + " category's " + shopItem.getName() +
                                " item's conditions can't be parsed!");
                        continue;
                    }
                    parsedPlaceholders.put(condition.getPlaceholder(), value);
                }
                if (value < condition.getMin() || value > condition.getMax()) continue;

                double percentage = value / (condition.getMax() + condition.getMin());

                possibleItems.put(shopItem, percentage);
            }
        }
        return possibleItems;
    }
}
