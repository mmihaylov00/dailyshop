package com.divictusgaming.dailyShop.storage;

import com.divictusgaming.dailyShop.gui.Menu;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.UUID;

public class MenuStorage {
    private final HashMap<UUID, SoftReference<Menu>> menus;

    public MenuStorage() {
        this.menus = new HashMap<>();
    }

    public void setLastOpen(Player p, Menu m) {
        menus.put(p.getUniqueId(), new SoftReference<>(m));
    }

    @Nullable
    public Menu getLastOpen(UUID uuid) {
        SoftReference<Menu> menuSoftReference = menus.get(uuid);
        if (menuSoftReference == null) return null;
        if (menuSoftReference.get() == null) menus.remove(uuid);
        return menuSoftReference.get();
    }

    public void leave(Player p) {
        menus.remove(p.getUniqueId());
    }
}
