package com.divictusgaming.dailyShop.storage;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.command.OpenCategoryCommand;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.database.configuration.ShopItemConfiguration;
import com.divictusgaming.dailyShop.database.implementation.FileDatabase;
import com.divictusgaming.dailyShop.exception.AirInHandException;
import com.divictusgaming.dailyShop.exception.CategoryNotFoundException;
import com.divictusgaming.dailyShop.exception.ExistingCategoryException;
import com.divictusgaming.dailyShop.exception.ExistingCommandCategoryException;
import com.divictusgaming.dailyShop.gui.menu.CategoryMenu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class CategoryStorage {
    private final HashMap<String, CategoryConfiguration> categories;

    public CategoryStorage() {
        this.categories = new HashMap<>();
        loadCategories();
    }

    private void loadCategories() {
        new FileDatabase()
                .loadAll(CategoryConfiguration.class, (result, results) -> {
                    for (CategoryConfiguration configuration : results) {
                        if (configuration.getCommand() != null) {
                            try {
                                Main.getPlugin(Main.class).getCommandManager().registerCommand(configuration.getCommand(),
                                        new OpenCategoryCommand(configuration));
                                categories.put(configuration.getName(), configuration);
                            } catch (ExistingCommandCategoryException ignored) {
                                //we don't register commands that are already registered
                            }
                        }
                    }
                });
    }

    public Set<String> getCategoryNames() {
        return categories.keySet();
    }

    public Collection<CategoryConfiguration> getCategories() {
        return categories.values();
    }

    public void saveCategory(CategoryConfiguration category) {
        category.setUpdated(true);
        new FileDatabase().saveData("name", category.getName(), category);
    }

    public void createCategory(ItemStack item, String name, int position, String command) throws
            ExistingCategoryException, ExistingCommandCategoryException {
        if (categories.containsKey(name)) throw new ExistingCategoryException();
        CategoryConfiguration category = new CategoryConfiguration(item, name, position, command);
        Main.getPlugin(Main.class).getCommandManager().registerCommand(command, new OpenCategoryCommand(category));
        categories.put(name, category);

        saveCategory(category);
    }

    public void deleteCategory(String name) throws CategoryNotFoundException {
        CategoryConfiguration category = categories.remove(name);
        if (category == null) throw new CategoryNotFoundException();

        Main.getPlugin(Main.class).getCommandManager().unregisterCommand(category.getPlainCommand());
        Main.getPlugin(Main.class).getItemCreationWizard().deleteCategory(name);

        Main.getPlugin(Main.class).getDatabase().deleteData("name", name, category);
    }

    public void editCategory(String name, ItemStack item) throws CategoryNotFoundException, AirInHandException {
        if (item.getType() == Material.AIR) throw new AirInHandException();
        CategoryConfiguration category = getCategory(name);
        category.setItem(item);

        saveCategory(category);
    }

    public void editCategory(String name, List<Integer> slots) throws CategoryNotFoundException {
        CategoryConfiguration category = getCategory(name);
        category.setItemSlots(slots);

        saveCategory(category);
    }

    public void addItem(String name, ShopItemConfiguration item) throws CategoryNotFoundException {
        CategoryConfiguration category = getCategory(name);
        category.getShopItems().add(item);

        saveCategory(category);
    }

    public boolean deleteItem(String name, String item) throws CategoryNotFoundException {
        CategoryConfiguration category = getCategory(name);
        boolean removed = category.getShopItems().removeIf(c -> c.getName().equals(item));

        if (removed) saveCategory(category);
        return removed;
    }

    public void editCategory(String name, int slot) throws CategoryNotFoundException {
        CategoryConfiguration category = getCategory(name);
        category.setPosition(slot);

        saveCategory(category);
    }

    public void editCategoryPermission(String name, String permission) throws CategoryNotFoundException {
        CategoryConfiguration category = getCategory(name);
        if (permission.equals("none")) permission = null;
        category.setPermission(permission);

        saveCategory(category);
    }

    public void editCategory(String name, String command) throws
            CategoryNotFoundException, ExistingCommandCategoryException {
        CategoryConfiguration category = getCategory(name);

        Main.getPlugin(Main.class).getCommandManager().registerCommand(command, new OpenCategoryCommand(category));
        Main.getPlugin(Main.class).getCommandManager().unregisterCommand(category.getPlainCommand());
        category.setCommand(command);

        saveCategory(category);
    }

    public CategoryConfiguration getCategory(String name) throws CategoryNotFoundException {
        CategoryConfiguration category = categories.get(name);
        if (category == null) throw new CategoryNotFoundException();

        return category;
    }

    public int getCategoriesCount(){
        return categories.size();
    }

    public void openCategoryGUI(Player player, String category) {
        try {
            new CategoryMenu(player, getCategory(category));
        } catch (CategoryNotFoundException e) {
        }
    }
}
