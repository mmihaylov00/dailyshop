package com.divictusgaming.dailyShop.command;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

public class OpenCategoryCommand extends BukkitCommand {
    private String category;

    OpenCategoryCommand(String name) {
        super(name);
    }

    public OpenCategoryCommand(CategoryConfiguration category) {
        this(category.getPlainCommand());
        this.category = category.getName();
        if (category.getPermission() != null) this.setPermission(category.getPermission());
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {

        if (commandSender instanceof Player)
            Main.getPlugin(Main.class).getCategoryStorage().openCategoryGUI((Player) commandSender, category);

        return true;
    }
}
