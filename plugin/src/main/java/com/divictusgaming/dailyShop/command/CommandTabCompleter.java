package com.divictusgaming.dailyShop.command;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.ShopItemConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.CurrencyYAMLConfiguration;
import com.divictusgaming.dailyShop.enumerate.Permission;
import com.divictusgaming.dailyShop.exception.CategoryNotFoundException;
import com.divictusgaming.dailyShop.exception.NotInWizardException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandTabCompleter implements TabCompleter {
    private final ArgumentNode argument;

    private static final CommandList[] commands = CommandList.values();

    private enum CommandList {
        RELOAD("reload", Permission.ADMIN),
        CATEGORY_CREATE("category create <name> <slot> <command>", Permission.ADMIN),
        CATEGORY_EDIT_ITEM("category edit item <category-name>", Permission.ADMIN),
        CATEGORY_LIST("category list", Permission.ADMIN),
        CATEGORY_EDIT_SLOT("category edit slot <category-name> <slot>", Permission.ADMIN),
        CATEGORY_EDIT_SLOTS("category edit set-slots <category-name> <slot> <slot> <slot> <slot> <slot> <slot> <slot> <slot>", Permission.ADMIN),
        CATEGORY_EDIT_PERMISSION("category edit permission <category-name> <permission>", Permission.ADMIN),
        CATEGORY_EDIT_PERMISSION_NONE("category edit permission <category-name> none", Permission.ADMIN),
        CATEGORY_EDIT_COMMAND("category edit command <category-name> <command>", Permission.ADMIN),
        CATEGORY_DELETE("category delete <category-name>", Permission.ADMIN),
        ITEM_CREATE("item create <name> <category-name>", Permission.ADMIN),
        ITEM_SAVE("item save", Permission.ADMIN),
        ITEM_CANCEL("item cancel", Permission.ADMIN),
        ITEM_OPEN("item open", Permission.ADMIN),
        ITEM_DELETE("item delete <category-name> <item-name>", Permission.ADMIN),
        ITEM_EDIT("item edit <category-name> <item-name>", Permission.ADMIN),
        ITEM_REMOVE_VARIABLE("item remove variable <variable-num>", Permission.ADMIN),
        ITEM_REMOVE_CONDITION("item remove condition <condition-num>", Permission.ADMIN),
        ITEM_REMOVE_PRICE("item remove price <price-num>", Permission.ADMIN),
        ITEM_REMOVE_LORE("item remove lore <row-num>", Permission.ADMIN),
        ITEM_LIST_PRICES("item list prices", Permission.ADMIN),
        ITEM_LIST_VARIABLES("item list variables", Permission.ADMIN),
        ITEM_LIST_CONDITIONS("item list conditions", Permission.ADMIN),
        ITEM_LIST_LORE("item list lore", Permission.ADMIN),
        ITEM_SET_CHANCE("item set chance <percentage>", Permission.ADMIN),
        ITEM_SET_COMMAND_PURCHASE_ITEM("item set command #purchase-item <amount> <enchant:level>", Permission.ADMIN),
        ITEM_SET_COMMAND("item set command <command>", Permission.ADMIN),
        ITEM_SET_DISPLAY_NAME("item set display-name <name>", Permission.ADMIN),
        ITEM_SET_MATERIAL("item set material", Permission.ADMIN),
        ITEM_SET_HIDE_TAGS_TRUE("item set hide-tags true", Permission.ADMIN),
        ITEM_SET_HIDE_TAGS_FALSE("item set hide-tags false", Permission.ADMIN),
        ITEM_SET_AMOUNT("item set amount <amount>", Permission.ADMIN),
        ITEM_ADD_CONDITION("item add condition <placeholder> <min> <max>", Permission.ADMIN),
        ITEM_ADD_PRICE("item add price <currency(min-max)> <currency(min-max)> <currency(min-max)> <currency(min-max)> <currency(min-max)>", Permission.ADMIN),
        ITEM_ADD_LORE("item add lore <lore>", Permission.ADMIN),
        ITEM_ADD_VARIABLE("item add variable <variable> <min> <max>", Permission.ADMIN),
        ;

        private final String command;
        private final Permission permission;

        CommandList(String command, Permission permission) {
            this.command = command;
            this.permission = permission;
        }
    }

    public CommandTabCompleter() {
        argument = new ArgumentNode(null, null);
        setupArguments();
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        Main plugin = Main.getPlugin(Main.class);
        if (args.length == 1) {
            ArrayList<String> suggestions = new ArrayList<>(argument.children.size());
            for (ArgumentNode child : argument.children)
                suggestions.add(child.arg);

            filterSuggestions(suggestions, args);
            suggestions.sort(String::compareTo);

            return suggestions;
        }

        List<String> suggestions = getSuggestions(argument, args, 0, commandSender);
        if (suggestions.remove("<item-name>"))
            try {
                for (ShopItemConfiguration shopItem : plugin.getCategoryStorage().getCategory(args[args.length - 2]).getShopItems())
                    suggestions.add(shopItem.getName());

            } catch (CategoryNotFoundException ignored) {
                suggestions.add("Invalid category name!");
            }
        else if (suggestions.remove("<category-name>"))
            suggestions.addAll(plugin.getCategoryStorage().getCategoryNames());
        else if (suggestions.contains("<slot>")) {
            suggestions.add("0");
            suggestions.add("" + (plugin.getConfiguration().getMenuRows() * 9 - 1));
        } else if (suggestions.remove("<amount>")) {
            suggestions.add("1");
            suggestions.add("64");
        } else if (suggestions.remove("<currency(min-max)>")) {
            for (CurrencyYAMLConfiguration c : plugin.getConfiguration().getCurrencies())
                suggestions.add(c.getName() + "(");
        } else {
            try {
                if (suggestions.remove("<condition-num>"))
                    for (int i = 1; i <= plugin.getItemCreationWizard().getItem((Player) commandSender).getConditions().size(); i++)
                        suggestions.add(i + "");
                else if (suggestions.remove("<variable-num>"))
                    for (int i = 1; i <= plugin.getItemCreationWizard().getItem((Player) commandSender).getVariables().size(); i++)
                        suggestions.add(i + "");
                else if (suggestions.remove("<price-num>"))
                    for (int i = 1; i <= plugin.getItemCreationWizard().getItem((Player) commandSender).getPrices().size(); i++)
                        suggestions.add(i + "");
            } catch (NotInWizardException e) {
                suggestions.add("Not creating an item!");
            }
        }

        filterSuggestions(suggestions, args);
        suggestions.sort(String::compareTo);
        return suggestions;
    }

    private void filterSuggestions(List<String> suggestions, String[] args) {
        String lastArg;
        if (!(lastArg = args[args.length - 1]).isEmpty()) {
            for (int i = 0; i < suggestions.size(); i++)
                if (!suggestions.get(i).startsWith(lastArg))
                    suggestions.remove(i--);
        }

    }

    private List<String> getSuggestions(ArgumentNode current, String[] args, int i, CommandSender commandSender) {
        if (args.length >= i) {
            for (ArgumentNode argument : current.children) {
                if (commandSender.hasPermission(argument.permission.getPermission()) &&
                        (argument.arg.equals(args[i]) || argument.arg.charAt(0) == '<')) {
                    if (++i == args.length - 1) {
                        ArrayList<String> suggestions = new ArrayList<>(argument.children.size());
                        for (ArgumentNode child : argument.children)
                            suggestions.add(child.arg);

                        return suggestions;
                    }
                    return getSuggestions(argument, args, i, commandSender);
                }
            }
        }
        return new ArrayList<>();
    }

    private void setupArguments() {
        for (CommandList value : commands) {
            String[] args = value.command.split(" ");
            ArgumentNode currentNode = argument;

            for (String arg : args) {
                boolean addToChildren = true;
                for (ArgumentNode argument : currentNode.children) {
                    if (argument.arg.equals(arg)) {
                        currentNode = argument;
                        addToChildren = false;
                        break;
                    }
                }
                if (addToChildren) {
                    ArgumentNode argument = new ArgumentNode(arg, value.permission);
                    currentNode.children.add(argument);
                    currentNode = argument;
                }
            }
        }
    }

    private static class ArgumentNode {
        private final String arg;
        private final Permission permission;
        private final List<ArgumentNode> children;

        public ArgumentNode(String arg, Permission permission) {
            this.arg = arg;
            this.permission = permission;
            this.children = new ArrayList<>();
        }
    }
}
