package com.divictusgaming.dailyShop.command;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.*;
import com.divictusgaming.dailyShop.database.configuration.sub.ItemCostConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.ItemCostWrapperConfiguration;
import com.divictusgaming.dailyShop.enumerate.Permission;
import com.divictusgaming.dailyShop.exception.*;
import com.divictusgaming.dailyShop.gui.menu.ItemEditorMenu;
import com.divictusgaming.dailyShop.gui.menu.MainMenu;
import com.divictusgaming.dailyShop.util.StringFormatter;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainCommand implements CommandExecutor {
    private MessagesYAMLConfiguration messages;

    private final Main plugin;

    public MainCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        messages = plugin.getMessages();
        if (args.length > 0)
            if (commandSender instanceof Player)
                playerCommand((Player) commandSender, args);
            else
                consoleCommand((ConsoleCommandSender) commandSender, args);
        else if (commandSender instanceof Player) new MainMenu((Player) commandSender);

        return true;
    }

    private void playerCommand(Player player, String[] args) {
        switch (args[0].toLowerCase()) {
            case "help":
                if (!player.hasPermission(Permission.ADMIN.getPermission())) {
                    sendNoPermission(player);
                    break;
                }
                sendHelp(player, messages.getAdminHelp());
                break;
            case "reload":
                reload(player);
                break;
            case "item":
                if (!player.hasPermission(Permission.ADMIN.getPermission())) {
                    sendNoPermission(player);
                    break;
                }
                if (args.length < 2) {
                    sendHelp(player, messages.getItemHelp());
                    break;
                }

                switch (args[1].toLowerCase()) {
                    case "create":
                        createItem(player, args);
                        break;
                    case "open":
                        openEditMenu(player);
                        break;
                    case "save":
                        saveItem(player);
                        break;
                    case "edit":
                        editItem(player, args);
                        break;
                    case "remove":
                        removeItemParams(player, args);
                        break;
                    case "delete":
                        deleteItem(player, args);
                        break;
                    case "cancel":
                        cancelItem(player);
                        break;
                    case "set":
                        setItemParam(player, args);
                        break;
                    case "list":
                        listItemValues(player, args);
                        break;
                    case "add":
                        addItemParam(player, args);
                        break;
                    default:
                        sendHelp(player, messages.getItemHelp());
                        break;
                }
                break;
            case "category":
                if (!player.hasPermission(Permission.ADMIN.getPermission())) {
                    sendNoPermission(player);
                    break;
                }
                if (args.length < 2) {
                    sendHelp(player, messages.getCategoryHelp());
                    break;
                }
                switch (args[1].toLowerCase()) {
                    case "create":
                        createCategory(player, args);
                        break;
                    case "list":
                        listCategories(player);
                        break;
                    case "edit":
                        editCategory(player, args);
                        break;
                    case "list-items":
                        listCategoryItems(player, args);
                        break;
                    case "delete":
                        deleteCategory(player, args);
                        break;
                    default:
                        sendHelp(player, messages.getCategoryHelp());
                        break;
                }
                break;
            default:
                StringFormatter.sendChatMessage(player, messages.getUnknownCommand());
        }
    }

    private void listItemValues(Player player, String[] args) {
        if (args.length < 3) {
            StringFormatter.sendChatMessage(player, messages.getItemListUsage());
            return;
        }
        try {
            switch (args[2].toLowerCase()) {
                case "conditions":
                    List<ConditionConfiguration> conditions = plugin.getItemCreationWizard().getItem(player).getConditions();
                    StringFormatter.sendChatMessage(player, messages.getItemListConditionsTitle());
                    for (int i = 0; i < conditions.size(); i++)
                        StringFormatter.sendChatMessage(player,
                                StringFormatter.replacePlaceholders(player, messages.getItemListConditionsRow(conditions.get(i).toString(), i + 1)));
                    break;
                case "prices":
                    List<ItemCostWrapperConfiguration> prices = plugin.getItemCreationWizard().getItem(player).getPrices();
                    StringFormatter.sendChatMessage(player, messages.getItemListPricesTitle());
                    for (int i = 0; i < prices.size(); i++)
                        StringFormatter.sendChatMessage(player, messages.getItemListPricesRow(prices.get(i).toString(), i + 1));
                    break;
                case "lore":
                    List<String> lore = plugin.getItemCreationWizard().getItem(player).getLore();
                    StringFormatter.sendChatMessage(player, messages.getItemListLoreTitle());
                    for (int i = 0; i < lore.size(); i++)
                        StringFormatter.sendChatMessage(player, messages.getItemListLoreRow(lore.get(i), i + 1));
                    break;
                case "variables":
                    List<VariableConfiguration> variables = plugin.getItemCreationWizard().getItem(player).getVariables();
                    StringFormatter.sendChatMessage(player, messages.getItemListVariablesTitle());
                    for (int i = 0; i < variables.size(); i++)
                        StringFormatter.sendChatMessage(player, messages.getItemListVariablesRow(variables.get(i).toString(), i + 1));
                    break;
                default:
                    StringFormatter.sendChatMessage(player, messages.getItemListUsage());
            }
        } catch (NotInWizardException e) {
            StringFormatter.sendChatMessage(player, messages.getNotInWizard());
        }
    }

    private void listCategoryItems(Player player, String[] args) {
        if (args.length < 3) {
            StringFormatter.sendChatMessage(player, messages.getListCategoryItemsUsage());
            return;
        }
        try {
            StringFormatter.sendChatMessage(player, messages.getListCategoryItems(args[2],
                    plugin.getCategoryStorage().getCategory(args[2]).getShopItemNames()));
        } catch (CategoryNotFoundException e) {
            StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
        }
    }

    private void editItem(Player player, String[] args) {
        if (args.length != 4) {
            StringFormatter.sendChatMessage(player, messages.getItemDeleteUsage());
            return;
        }

        try {
            Main plugin = Main.getPlugin(Main.class);
            CategoryConfiguration category = plugin.getCategoryStorage().getCategory(args[2]);

            ShopItemConfiguration configuration = null;
            for (ShopItemConfiguration c : category.getShopItems()) {
                if (c.getName().equals(args[3])) {
                    configuration = c;
                    break;
                }
            }

            if (configuration == null) {
                StringFormatter.sendChatMessage(player, messages.getItemNotFoundError());
                return;
            }
            category.getShopItems().remove(configuration);
            plugin.getItemCreationWizard().addToEdit(player, configuration);
            new ItemEditorMenu(player);
        } catch (CategoryNotFoundException e) {
            StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
        }
    }

    private void deleteItem(Player player, String[] args) {
        if (args.length != 4) {
            StringFormatter.sendChatMessage(player, messages.getItemDeleteUsage());
            return;
        }

        try {
            if (plugin.getCategoryStorage().deleteItem(args[2], args[3]))
                StringFormatter.sendChatMessage(player, messages.getItemRemoveSuccessful());
            else
                StringFormatter.sendChatMessage(player, messages.getItemNotFoundError());
        } catch (CategoryNotFoundException e) {
            StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
        }
    }

    private void removeItemParams(Player player, String[] args) {
        if (args.length != 4) {
            StringFormatter.sendChatMessage(player, messages.getItemRemoveParamUsage());
            return;
        }

        try {
            switch (args[2].toLowerCase()) {
                case "variable":
                    plugin.getItemCreationWizard().getItem(player).getVariables().remove(Integer.parseInt(args[3]) - 1);
                    StringFormatter.sendChatMessage(player, messages.getItemRemoveParamVariableRemoved());
                    listItemValues(player, new String[]{null, null, "variables"});
                    break;
                case "lore":
                    plugin.getItemCreationWizard().getItem(player).removeLoreLine(Integer.parseInt(args[3]) - 1);
                    StringFormatter.sendChatMessage(player, messages.getItemRemoveParamLoreLineRemoved());
                    listItemValues(player, new String[]{null, null, "lore"});
                    break;
                case "condition":
                    plugin.getItemCreationWizard().getItem(player).getConditions().remove(Integer.parseInt(args[3]) - 1);
                    StringFormatter.sendChatMessage(player, messages.getItemRemoveParamConditionRemoved());
                    listItemValues(player, new String[]{null, null, "conditions"});
                    break;
                case "price":
                    plugin.getItemCreationWizard().getItem(player).getPrices().remove(Integer.parseInt(args[3]) - 1);
                    StringFormatter.sendChatMessage(player, messages.getItemRemoveParamPriceRemoved());
                    listItemValues(player, new String[]{null, null, "prices"});
                    break;
                default:
                    StringFormatter.sendChatMessage(player, messages.getItemRemoveParamUsage());
            }

        } catch (NotInWizardException e) {
            StringFormatter.sendChatMessage(player, messages.getNotInWizard());
        } catch (NumberFormatException e) {
            StringFormatter.sendChatMessage(player, messages.getItemRemoveParamUsage());
        } catch (IndexOutOfBoundsException e) {
            StringFormatter.sendChatMessage(player, messages.getItemRemoveParamInvalidNum());
        }
    }

    private static final Pattern PRICE_PATTERN = Pattern.compile("([a-z_]+)\\(([0-9.]+)-([0-9.]+)\\)");

    private void addItemParam(Player player, String[] args) {
        try {
            switch (args[2].toLowerCase()) {
                case "price":
                    if (args.length < 4) {
                        StringFormatter.sendChatMessage(player, messages.getItemPriceUsage());
                        break;
                    }
                    try {
                        Main plugin = Main.getPlugin(Main.class);
                        ShopItemConfiguration item = plugin.getItemCreationWizard().getItem(player);

                        ItemCostWrapperConfiguration costs = new ItemCostWrapperConfiguration();

                        for (int i = 3; i < args.length; i++) {
                            final Matcher matcher = PRICE_PATTERN.matcher(args[i]);
                            if (matcher.find()) {

                                String currency = matcher.group(1);
                                if (plugin.getConfiguration().getCurrencies().stream().noneMatch(c -> c.getName().equals(currency))) {
                                    StringFormatter.sendChatMessage(player, messages.getUnknownCurrency(currency));
                                    return;
                                }

                                double min = Double.parseDouble(matcher.group(2));
                                double max = Double.parseDouble(matcher.group(3));
                                if (min > max) {
                                    StringFormatter.sendChatMessage(player, messages.getItemValueMinMoreThanMax());
                                    return;
                                }

                                if (costs.getPrices().stream().anyMatch(c -> c.getCurrencyName().equals(currency))) {
                                    StringFormatter.sendChatMessage(player, messages.getItemPriceDuplicated(currency));
                                    return;
                                }

                                costs.getPrices().add(new ItemCostConfiguration(currency, min, max));
                            } else {
                                StringFormatter.sendChatMessage(player, messages.getItemPriceUsage());
                                return;
                            }
                        }
                        item.getPrices().add(costs);

                        StringFormatter.sendChatMessage(player, messages.getItemPriceAdded());
                        new ItemEditorMenu(player);
                    } catch (NotInWizardException e) {
                        StringFormatter.sendChatMessage(player, messages.getNotInWizard());
                    }
                    break;
                case "variable":
                    if (args.length != 6) {
                        StringFormatter.sendChatMessage(player, messages.getItemVariableUsage());
                        break;
                    }
                    try {
                        ShopItemConfiguration item = plugin.getItemCreationWizard().getItem(player);

                        String v = args[3].replaceAll("[{}]", "");
                        if (item.getVariables().stream().anyMatch(var -> var.getVariable().equals(v))) {
                            StringFormatter.sendChatMessage(player, messages.getItemVariableExists());
                            return;
                        }

                        int min = Integer.parseInt(args[4]);
                        int max = Integer.parseInt(args[5]);
                        if (min > max) {
                            StringFormatter.sendChatMessage(player, messages.getItemValueMinMoreThanMax());
                            return;
                        }

                        item.getVariables().add(new VariableConfiguration(v, min, max));
                        StringFormatter.sendChatMessage(player, messages.getItemVariableAdded());
                        new ItemEditorMenu(player);
                    } catch (NotInWizardException e) {
                        StringFormatter.sendChatMessage(player, messages.getNotInWizard());
                    }
                    break;
                case "condition":
                    if (args.length != 6) {
                        StringFormatter.sendChatMessage(player, messages.getItemConditionUsage());
                        break;
                    }
                    try {
                        ShopItemConfiguration item = plugin.getItemCreationWizard().getItem(player);

                        String placeholder = args[3].replaceAll("%", "");

                        if (item.getConditions().stream().anyMatch(var -> var.getPlaceholder().equals(placeholder))) {
                            StringFormatter.sendChatMessage(player, messages.getItemConditionExists());
                            return;
                        }

                        try {
                            Double.parseDouble(PlaceholderAPI.setPlaceholders(player, "%" + placeholder + "%"));
                        } catch (NumberFormatException e) {
                            StringFormatter.sendChatMessage(player, messages.getItemPlaceholderNaN());
                            return;
                        }

                        double min = Double.parseDouble(args[4]);
                        double max = Double.parseDouble(args[5]);

                        if (min > max) {
                            StringFormatter.sendChatMessage(player, messages.getItemValueMinMoreThanMax());
                            return;
                        }

                        item.getConditions().add(new ConditionConfiguration(placeholder, min, max));
                        StringFormatter.sendChatMessage(player, messages.getItemVariableAdded());
                        new ItemEditorMenu(player);
                    } catch (NotInWizardException e) {
                        StringFormatter.sendChatMessage(player, messages.getNotInWizard());
                    }
                    break;
                case "lore":
                    if (args.length < 4) {
                        StringFormatter.sendChatMessage(player, messages.getItemLoreUsage());
                        break;
                    }
                    try {
                        ShopItemConfiguration item = plugin.getItemCreationWizard().getItem(player);
                        StringJoiner joiner = new StringJoiner(" ");

                        for (int i = 3; i < args.length; i++)
                            joiner.add(args[i]);
                        String lore = joiner.toString();

                        item.addLoreLine(lore);
                        StringFormatter.sendChatMessage(player, messages.getItemLoreAdded());
                        new ItemEditorMenu(player);
                    } catch (NotInWizardException e) {
                        StringFormatter.sendChatMessage(player, messages.getNotInWizard());
                    }
                    break;
                default:
                    StringFormatter.sendChatMessage(player, messages.getItemDeleteUsage());
            }
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            sendHelp(player, messages.getItemHelp());
        }
    }

    private void setItemParam(Player player, String[] args) {
        try {
            try {
                StringJoiner joiner = new StringJoiner(" ");
                ShopItemConfiguration item;
                switch (args[2].toLowerCase()) {
                    case "chance":
                        if (args.length != 4) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemChanceUsage());
                            break;
                        }
                        item = plugin.getItemCreationWizard().getItem(player);
                        try {
                            double chance = Double.parseDouble(args[3]);
                            if (chance <= 0) {
                                StringFormatter.sendChatMessage(player, messages.getItemChanceNegativeOrZero());
                                return;
                            }

                            item.setPercentage(chance);
                        } catch (NumberFormatException e) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemChanceUsage());
                            break;
                        }
                        StringFormatter.sendChatMessage(player, messages.getItemChanceSet());
                        new ItemEditorMenu(player);
                        break;
                    case "display-name":
                        if (args.length < 4) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemDisplayNameUsage());
                            break;
                        }
                        for (int i = 3; i < args.length; i++)
                            joiner.add(args[i]);
                        String name = joiner.toString();

                        item = plugin.getItemCreationWizard().getItem(player);
                        item.setDisplayName(name);

                        StringFormatter.sendChatMessage(player, messages.getItemDisplayNameSet());
                        new ItemEditorMenu(player);
                        break;
                    case "material":
                        if (args.length != 3) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemMaterialUsage());
                            break;
                        }

                        item = plugin.getItemCreationWizard().getItem(player);
                        item.setMaterial(player.getItemInHand());

                        StringFormatter.sendChatMessage(player, messages.getItemMaterialSet());
                        new ItemEditorMenu(player);
                        break;
                    case "hide-tags":
                        item = plugin.getItemCreationWizard().getItem(player);
                        item.setHideTags(args.length == 4 ? args[3].toLowerCase(Locale.ROOT).equals("true") : !item.isHideTags());

                        StringFormatter.sendChatMessage(player, messages.getItemHideTagsSet());
                        new ItemEditorMenu(player);
                        break;
                    case "amount":
                        if (args.length != 4) {
                            StringFormatter.sendChatMessage(player, messages.getSetAmountUsage());
                            break;
                        }
                        try {
                            int amount = Integer.parseInt(args[3]);
                            item = plugin.getItemCreationWizard().getItem(player);
                            item.setAmount(amount);
                        } catch (NumberFormatException e) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemChanceUsage());
                            break;
                        }

                        StringFormatter.sendChatMessage(player, messages.getItemAmountSet());
                        new ItemEditorMenu(player);
                        break;
                    case "command":
                        if (args.length < 4) {
                            StringFormatter.sendChatMessage(player, messages.getSetItemCommandUsage());
                            break;
                        }
                        for (int i = 3; i < args.length; i++)
                            joiner.add(args[i]);
                        String command = joiner.toString();

                        if (!command.startsWith("#")) command = "#purchase-command " + command;

                        item = plugin.getItemCreationWizard().getItem(player);
                        item.setCommand(command);
                        StringFormatter.sendChatMessage(player, messages.getItemCommandSet());
                        new ItemEditorMenu(player);
                        break;
                    default:
                        StringFormatter.sendChatMessage(player, messages.getUnknownCommand());
                }
            } catch (NotInWizardException e) {
                StringFormatter.sendChatMessage(player, messages.getNotInWizard());
            }

        } catch (IndexOutOfBoundsException e) {
            sendHelp(player, messages.getItemHelp());
        }
    }

    private void cancelItem(Player player) {
        try {
            plugin.getItemCreationWizard().cancel(player);
            StringFormatter.sendChatMessage(player, messages.getItemCanceled());
        } catch (NotInWizardException e) {
            StringFormatter.sendChatMessage(player, messages.getNotInWizard());
        }
    }

    private void saveItem(Player player) {
        try {
            plugin.getItemCreationWizard().save(player);
            StringFormatter.sendChatMessage(player, messages.getItemSaved());
        } catch (NotInWizardException e) {
            StringFormatter.sendChatMessage(player, messages.getNotInWizard());
        } catch (EmptyConfigurationException e) {
            StringFormatter.sendChatMessage(player, messages.getItemNeedAtLeastOne(e.getName()));
        } catch (ConfigurationNotSetException e) {
            StringFormatter.sendChatMessage(player, messages.getItemMissingSetting(e.getName()));
        }
    }

    private void createItem(Player player, String[] args) {
        if (args.length != 4) {
            StringFormatter.sendChatMessage(player, messages.getCreateItemUsage());
        } else {
            try {
                plugin.getItemCreationWizard().create(player, args[2], args[3]);
                StringFormatter.sendChatMessage(player, messages.getCreateItemStarted());
            } catch (AirInHandException e) {
                StringFormatter.sendChatMessage(player, messages.getEmptyHandError());
            } catch (CategoryNotFoundException e) {
                StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
            } catch (ExistingItemException e) {
                StringFormatter.sendChatMessage(player, messages.getItemExists());
            } catch (AlreadyInWizardException e) {
                StringFormatter.sendChatMessage(player, messages.getAlreadyInWizard());
            }
        }
    }

    private void openEditMenu(Player player) {
        new ItemEditorMenu(player);
    }

    private void listCategories(Player player) {
        Set<String> categories = plugin.getCategoryStorage().getCategoryNames();
        StringFormatter.sendChatMessage(player, messages.getListCategory(String.join(", ", categories)));
    }

    private void editCategory(Player player, String[] args) {
        try {
            switch (args[2].toLowerCase()) {
                case "item":
                    try {
                        plugin.getCategoryStorage().editCategory(args[3], player.getInventory().getItemInHand());
                        StringFormatter.sendChatMessage(player, messages.getEditCategoryUpdated());
                    } catch (CategoryNotFoundException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
                    } catch (AirInHandException e) {
                        StringFormatter.sendChatMessage(player, messages.getEmptyHandError());
                    }
                    break;
                case "set-slots":
                    try {
                        List<Integer> slots = new ArrayList<>();
                        for (int i = 4; i < args.length; i++)
                            slots.add(Integer.parseInt(args[i]));

                        plugin.getCategoryStorage().editCategory(args[3], slots);
                        for (Player p : Bukkit.getOnlinePlayers())
                            plugin.getPlayerDataStorage().refreshCategory(p, args[3]);

                        StringFormatter.sendChatMessage(player, messages.getEditCategoryUpdated());
                    } catch (CategoryNotFoundException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
                    } catch (NumberFormatException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryInvalidSlot());
                    }
                    break;
                case "slot":
                    try {
                        plugin.getCategoryStorage().editCategory(args[3], Integer.parseInt(args[4]));
                        StringFormatter.sendChatMessage(player, messages.getEditCategoryUpdated());
                    } catch (CategoryNotFoundException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
                    }
                    break;
                case "permission":
                    try {
                        plugin.getCategoryStorage().editCategoryPermission(args[3], args[4]);
                        StringFormatter.sendChatMessage(player, messages.getEditCategoryUpdated());
                    } catch (CategoryNotFoundException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
                    }
                    break;
                case "command":
                    try {
                        if (args.length != 5) throw new IndexOutOfBoundsException();

                        plugin.getCategoryStorage().editCategory(args[3], args[4]);
                        StringFormatter.sendChatMessage(player, messages.getEditCategoryUpdated());
                    } catch (CategoryNotFoundException e) {
                        StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
                    } catch (ExistingCommandCategoryException e) {
                        StringFormatter.sendChatMessage(player, messages.getEditCategoryCommandRegistered());
                    }
                    break;
            }
        } catch (IndexOutOfBoundsException e) {
            sendHelp(player, messages.getCategoryHelp());
        } catch (NumberFormatException e) {
            StringFormatter.sendChatMessage(player, messages.getCategoryInvalidSlot());
        }
    }

    private void deleteCategory(Player player, String[] args) {
        if (args.length != 3)
            StringFormatter.sendChatMessage(player, messages.getDeleteCategoryUsage());
        else {
            try {
                plugin.getCategoryStorage().deleteCategory(args[2]);
                StringFormatter.sendChatMessage(player, messages.getDeleteCategoryDeleted());
            } catch (CategoryNotFoundException e) {
                StringFormatter.sendChatMessage(player, messages.getCategoryNotFoundError());
            }
        }
    }

    private void consoleCommand(ConsoleCommandSender console, String[] args) {
        switch (args[0].toLowerCase()) {
            case "reload":
                reload(console);
                break;
            case "help":
            default:
                for (String s : messages.getConsoleHelp())
                    console.sendMessage(s);
        }
    }

    private void sendHelp(Player p, List<String> help) {
        for (String s : help)
            StringFormatter.sendChatMessage(p, s);
    }

    private void sendNoPermission(Player player) {
        StringFormatter.sendChatMessage(player, messages.getNoPermissionError());
    }

    private void createCategory(Player player, String[] args) {
        if (args.length != 5)
            StringFormatter.sendChatMessage(player, messages.getCreateCategoryUsage());
        else if (player.getInventory().getItemInHand().getType() == Material.AIR)
            StringFormatter.sendChatMessage(player, messages.getEmptyHandError());
        else {
            try {
                plugin.getCategoryStorage().createCategory(player.getInventory().getItemInHand(),
                        args[2], Integer.parseInt(args[3]), args[4]);
                StringFormatter.sendChatMessage(player, messages.getCreateCategoryCreated());
            } catch (NumberFormatException e) {
                StringFormatter.sendChatMessage(player, messages.getCategoryInvalidSlot());
            } catch (ExistingCategoryException e) {
                StringFormatter.sendChatMessage(player, messages.getCreateCategoryAlreadyExists());
            } catch (ExistingCommandCategoryException e) {
                StringFormatter.sendChatMessage(player, messages.getEditCategoryCommandRegistered());
            }
        }
    }

    private void reload(CommandSender sender) {
        if (sender instanceof Player && !sender.hasPermission(Permission.ADMIN.getPermission()))
            sendNoPermission((Player) sender);
        else {
            sender.sendMessage(messages.getReloadStarted());
            plugin.reload(sender);
        }
    }
}
