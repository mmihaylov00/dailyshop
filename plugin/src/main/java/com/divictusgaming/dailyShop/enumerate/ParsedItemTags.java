package com.divictusgaming.dailyShop.enumerate;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.nms.IVersionMain;
import org.bukkit.inventory.ItemStack;

public class ParsedItemTags {
    private final String command;
    private final Boolean isFromMenu;

    public ParsedItemTags(IVersionMain.ItemTag tag) {
        this.command = tag.getCommand();
        this.isFromMenu = tag.getFromMenu();
    }

    public static ParsedItemTags getItemTags(ItemStack item) {
        IVersionMain.ItemTag tags = Main.getPlugin(Main.class).getVersionMain().getTags(item);
        if (tags == null) return null;
        return new ParsedItemTags(tags);
    }

    public String getCommand() {
        return command;
    }

    public Boolean getFromMenu() {
        return isFromMenu;
    }
}
