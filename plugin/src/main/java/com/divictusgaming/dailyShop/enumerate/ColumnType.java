package com.divictusgaming.dailyShop.enumerate;

public enum ColumnType {
    VARCHAR("VARCHAR(64)"),
    INT("INT"),
    DOUBLE("DOUBLE"),
    TEXT("TEXT"),
    BOOL("BOOL");

    private String mysqlType;

    ColumnType(String mysqlType) {
        this.mysqlType = mysqlType;
    }

    public String getMysqlType() {
        return mysqlType;
    }
}