package com.divictusgaming.dailyShop.enumerate;

public enum Permission {
    ADMIN("dailyshop.admin"),
            ;
    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
