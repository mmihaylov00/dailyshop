package com.divictusgaming.dailyShop;

import com.divictusgaming.dailyShop.command.CommandTabCompleter;
import com.divictusgaming.dailyShop.command.MainCommand;
import com.divictusgaming.dailyShop.database.IDatabase;
import com.divictusgaming.dailyShop.database.configuration.ConfigYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.MessagesYAMLConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.YAMLWorker;
import com.divictusgaming.dailyShop.database.implementation.FileDatabase;
import com.divictusgaming.dailyShop.database.implementation.SQLDatabase;
import com.divictusgaming.dailyShop.event.PlayerEvent;
import com.divictusgaming.dailyShop.exception.DataNotFoundException;
import com.divictusgaming.dailyShop.gui.event.InventoryClickEvent;
import com.divictusgaming.dailyShop.nms.IVersionMain;
import com.divictusgaming.dailyShop.papi.PluginPlaceholders;
import com.divictusgaming.dailyShop.storage.CategoryStorage;
import com.divictusgaming.dailyShop.storage.MenuStorage;
import com.divictusgaming.dailyShop.storage.PlayerDataStorage;
import com.divictusgaming.dailyShop.util.CommandManager;
import com.divictusgaming.dailyShop.util.RefreshTimer;
import com.divictusgaming.dailyShop.util.StringFormatter;
import com.divictusgaming.dailyShop.util.VersionChecker;
import com.divictusgaming.dailyShop.wizard.ItemCreationWizard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;

public class Main extends JavaPlugin {
    private File dataFolder;
    private IDatabase database;
    private MessagesYAMLConfiguration messages;
    private ConfigYAMLConfiguration config;
    private CategoryStorage categoryStorage;
    private CommandManager commandManager;
    private RefreshTimer timer;
    private PlayerDataStorage playerDataStorage;
    private ItemCreationWizard itemCreationWizard;
    private MenuStorage menuStorage;
    private IVersionMain versionMain;

    private static final ChatColor MAIN_COLOR = ChatColor.DARK_AQUA;

    @Override
    public void onEnable() {
        dataFolder = getDataFolder().getAbsoluteFile();

        if (!setupVersion()) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Your server version is not compatible with Daily Shop!");
            return;
        }
        commandManager = new CommandManager(this);
        reload(null);

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") == null)
            throw new RuntimeException("Could not find PlaceholderAPI!");
        else new PluginPlaceholders().register();

        this.getCommand("dailyshop").setExecutor(new MainCommand(this));
        this.getCommand("dailyshop").setTabCompleter(new CommandTabCompleter());
        Bukkit.getServer().getPluginManager().registerEvents(new InventoryClickEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerEvent(), this);

        itemCreationWizard = new ItemCreationWizard();
        playerDataStorage = new PlayerDataStorage();
        menuStorage = new MenuStorage();

        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "------------------------------------------");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "Daily Shop (v" + VersionChecker.VERSION + ")");
        try {
            if (VersionChecker.isLatestVersion()) {
                Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "You have the latest version!");
            } else {
                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "There is a newer version available!");
            }
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Could not check for newer version, please connect to the internet!");
        }
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "Plugin purchased by: %%__USER__%%");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "Spigot page: https://www.spigotmc.org/resources/%%__RESOURCE__%%/");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "Created by ZeaL_BGN");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "Spigot profile: https://www.spigotmc.org/members/51054/");
        Bukkit.getConsoleSender().sendMessage(MAIN_COLOR + "------------------------------------------");

        new BukkitRunnable() {
            @Override
            public void run() {
                if (timer != null) timer.testRefresh();
            }
        }.runTaskTimerAsynchronously(this, 0, 20);
    }

    public void reload(CommandSender sender) {
        //create a new messages file
        BukkitRunnable reload = new BukkitRunnable() {
            @Override
            public void run() {
                //create a new messages file
                commandManager.clearCommands();
                try {
                    messages = new MessagesYAMLConfiguration();
                    YAMLWorker.readFile(messages);
                    YAMLWorker.addMissing(messages);
                } catch (DataNotFoundException e) {
                    YAMLWorker.saveFile(messages);
                }
                StringFormatter.formatAllFields(messages);

                try {
                    config = new ConfigYAMLConfiguration();
                    YAMLWorker.readFile(config);
                    YAMLWorker.addMissing(config);
                } catch (DataNotFoundException e) {
                    YAMLWorker.saveFile(config);
                }
                StringFormatter.formatAllFields(config);

                connectToDb();
                categoryStorage = new CategoryStorage();

                if (sender != null) sender.sendMessage(messages.getReloadCompleted());

                timer = new RefreshTimer();

                if (!database.isConnected())
                    Bukkit.getConsoleSender().sendMessage(messages.getPrefix() + ChatColor.RED + "Could not connect to the Database!");
            }
        };

        if (sender == null) reload.runTask(this);
        else reload.runTaskAsynchronously(this);
    }

    private void connectToDb() {
        if (database != null)
            if (database instanceof SQLDatabase) ((SQLDatabase) database).closeConnection();

        switch (config.getDatabaseType().toLowerCase()) {
            case "mysql":
                database = new SQLDatabase();
                break;
            case "file":
            default:
                database = new FileDatabase();
        }

    }

    @Override
    public void onDisable() {
        playerDataStorage.saveAllPlayers();
        if (database instanceof SQLDatabase)
            ((SQLDatabase) database).closeConnection();

        getLogger().info("Daily Shop Disabled");
    }

    private boolean setupVersion() {
        String version;
        try {
            version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {
            return false;
        }
        switch (version) {
            case "v1_8_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_8_R1.VersionMain();
                break;
            case "v1_8_R2":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_8_R2.VersionMain();
                break;
            case "v1_8_R3":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_8_R3.VersionMain();
                break;
            case "v1_9_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_9_R1.VersionMain();
                break;
            case "v1_9_R2":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_9_R2.VersionMain();
                break;
            case "v1_10_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_10_R1.VersionMain();
                break;
            case "v1_11_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_11_R1.VersionMain();
                break;
            case "v1_12_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_12_R1.VersionMain();
                break;
            case "v1_13_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_13_R1.VersionMain();
                break;
            case "v1_13_R2":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_13_R2.VersionMain();
                break;
            case "v1_14_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_14_R1.VersionMain();
                break;
            case "v1_15_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_15_R1.VersionMain();
                break;
            case "v1_16_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_16_R1.VersionMain();
                break;
            case "v1_16_R2":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_16_R2.VersionMain();
                break;
            case "v1_16_R3":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_16_R3.VersionMain();
                break;
            case "v1_17_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_18_R1.VersionMain();
                break;
            case "v1_18_R1":
                versionMain = new com.divictusgaming.dailyShop.nms.v1_18_R1.VersionMain();
                break;
        }
        return versionMain != null;
    }

    public File getPluginsFolder() {
        return dataFolder;
    }

    public IDatabase getDatabase() {
        return database;
    }

    public MessagesYAMLConfiguration getMessages() {
        return messages;
    }

    public ConfigYAMLConfiguration getConfiguration() {
        return config;
    }

    public CategoryStorage getCategoryStorage() {
        return categoryStorage;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public RefreshTimer getTimer() {
        return timer;
    }

    public ItemCreationWizard getItemCreationWizard() {
        return itemCreationWizard;
    }

    public PlayerDataStorage getPlayerDataStorage() {
        return playerDataStorage;
    }

    public MenuStorage getMenuStorage() {
        return menuStorage;
    }

    public IVersionMain getVersionMain() {
        return versionMain;
    }
}
