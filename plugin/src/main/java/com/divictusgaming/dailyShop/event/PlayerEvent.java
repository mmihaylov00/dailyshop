package com.divictusgaming.dailyShop.event;

import com.divictusgaming.dailyShop.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerEvent implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Main plugin = Main.getPlugin(Main.class);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (plugin.getPlayerDataStorage() != null)
                    plugin.getPlayerDataStorage().loadPlayer(event.getPlayer().getUniqueId());
            }
        }.runTaskAsynchronously(plugin);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Main plugin = Main.getPlugin(Main.class);
        new BukkitRunnable() {
            @Override
            public void run() {
                plugin.getPlayerDataStorage().savePlayer(event.getPlayer().getUniqueId(), true);
                plugin.getMenuStorage().leave(event.getPlayer());
            }
        }.runTaskAsynchronously(plugin);
    }
}
