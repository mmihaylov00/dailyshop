package com.divictusgaming.dailyShop.gui;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.ConfigYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.DisplayItemConfiguration;
import com.divictusgaming.dailyShop.enumerate.ParsedItemTags;
import com.divictusgaming.dailyShop.gui.command.MenuCommand;
import com.divictusgaming.dailyShop.gui.menu.CategoryMenu;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.List;

public abstract class Menu implements InventoryHolder {
    private final Inventory inventory;
    private ArrayDeque<DisplayItemConfiguration> refreshedItems;
    private static final MenuCommand[] commands = MenuCommand.values();

    public Menu(String title) {
        inventory = Bukkit.createInventory(this, 9 * Main.getPlugin(Main.class).getConfiguration().getMenuRows(), title);
        this.refreshedItems = new ArrayDeque<>();
    }

    protected void putItem(Player player, ItemStack configItem, int position) {
        putItem(player, configItem, position, null);
    }

    protected void putItem(Player player, ItemStack configItem, int position, HashMap<String, String> additionalPlaceholders) {
        ItemStack item = new ItemStack(configItem);
        ItemMeta meta = item.getItemMeta();

        try {
            List<String> lore = meta.getLore();
            if (lore != null && !lore.isEmpty())
                meta.setLore(StringFormatter.replacePlaceholders(player, lore, additionalPlaceholders));
            meta.setDisplayName(StringFormatter.replacePlaceholders(player, meta.getDisplayName(), additionalPlaceholders));
            item.setItemMeta(meta);
        } catch (NullPointerException ignored) {
        }

        inventory.setItem(position, item);
    }

    protected void putItems(final Player player, List<? extends DisplayItemConfiguration> items) {
        for (final DisplayItemConfiguration item : items) {
            putItem(player, item.getItem(), item.getPosition());
            if (item.getCommand().startsWith("#refresh"))
                refreshedItems.add(item);

        }
    }

    private void fillEmpty(ItemStack item) {
        if (item.getType() != Material.AIR) {
            ItemStack[] content = inventory.getContents();
            for (int i = 0; i < content.length; i++) {
                if (content[i] == null || content[i].getType() == Material.AIR)
                    content[i] = item;
            }
            inventory.setContents(content);
        }
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public void handleClickEvent(org.bukkit.event.inventory.InventoryClickEvent event,
                                 ParsedItemTags tags) {
        event.setResult(Event.Result.DENY);
        event.setCancelled(true);

        if (tags.getCommand() != null && !tags.getCommand().isEmpty()) {
            Player player = (Player) event.getWhoClicked();

            if (tags.getCommand().charAt(0) == '#')
                for (MenuCommand value : commands)
                    if (tags.getCommand().startsWith(value.getCommand())) {

                        if (this instanceof CategoryMenu) {
                            value.setCategory(((CategoryMenu) this).getCategory());
                            value.setItem(((CategoryMenu) this).getItems().get(event.getSlot()));
                        } else {
                            value.setCategory(null);
                            value.setItem(null);
                        }

                        value.executeCommand(player);
                        return;
                    }

            String command = StringFormatter.replacePlaceholders(player, tags.getCommand());
            if (command.charAt(0) == '/') {
                if (tags.getCommand() != null) event.getWhoClicked().closeInventory();
                Bukkit.dispatchCommand(event.getWhoClicked(), command.substring(1));
            }
            else
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        }
    }

    public void openInventory(final Player p) {
        Main plugin = Main.getPlugin(Main.class);
        ConfigYAMLConfiguration config = plugin.getConfiguration();
        if (config.isFillEmpty())
            fillEmpty(config.getFillItem().getItem());

        p.openInventory(inventory);
        plugin.getMenuStorage().setLastOpen(p, this);

        final InventoryHolder t = this;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (refreshedItems != null && p.getOpenInventory().getTopInventory().getHolder() == t)
                    for (DisplayItemConfiguration i : refreshedItems)
                        if (i != null) putItem(p, i.getItem(), i.getPosition());
                else
                    this.cancel();

            }
        }.runTaskTimerAsynchronously(Main.getPlugin(Main.class), 20, 20);
    }

    public abstract void refreshInventory(Player player);
}
