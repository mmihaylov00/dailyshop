package com.divictusgaming.dailyShop.gui.menu;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.gui.Menu;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class MainMenu extends Menu {

    public MainMenu(Player player) {
        super(Main.getPlugin(Main.class).getConfiguration().getMainMenuTitle());

        refreshInventory(player);
        openInventory(player);
    }

    @Override
    public void refreshInventory(Player player) {
        final Main plugin = Main.getPlugin(Main.class);
        new BukkitRunnable() {

            @Override
            public void run() {
                putItems(player, plugin.getConfiguration().getMainMenuItems());

                for (CategoryConfiguration category : plugin.getCategoryStorage().getCategories())
                    if (category.getPermission() == null || category.getPermission().isEmpty() ||
                            player.hasPermission(category.getPermission()))
                        putItem(player, category.getItem(), category.getPosition());
            }
        }.runTaskAsynchronously(plugin);
    }
}
