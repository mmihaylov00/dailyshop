package com.divictusgaming.dailyShop.gui.command;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.database.configuration.PlayerShopItemConfiguration;
import com.divictusgaming.dailyShop.database.configuration.ShopItemConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.CurrencyYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.PlayerShopItemPriceConfiguration;
import com.divictusgaming.dailyShop.util.StringFormatter;
import com.divictusgaming.dailyShop.util.UMaterial;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public enum MenuCommand {
    CLOSE("#close", (p, category, item) -> {
        p.closeInventory();
    }),
    PURCHASE_ITEM("#purchase-item", (p, category, item) -> {
        ShopItemConfiguration shopItem = null;
        for (ShopItemConfiguration c : category.getShopItems()) {
            if (item.getName() != null && c.getName() != null && c.getName().equals(item.getName())) {
                shopItem = c;
                break;
            }
        }

        if (shopItem == null) return;

        Main plugin = Main.getPlugin(Main.class);
        if (p.getInventory().getSize() == 36) {
            int i = 0;
            for (; i < 36; i++) {
                if (p.getInventory().getItem(i) == null)
                    break;
            }
            if (i == 36) {

                if (shopItem.getItem().getMaxStackSize() == 1) {
                    StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseNotEnoughSpace());
                    return;
                }

                int slot;
                if ((slot = p.getInventory().first(shopItem.getItem())) == -1) {
                    StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseNotEnoughSpace());
                    return;
                }

                ItemStack itemStack = p.getInventory().getItem(slot);
                if (itemStack.getMaxStackSize() - itemStack.getAmount() < shopItem.getAmount()) {
                    StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseNotEnoughSpace());
                    return;
                }
            }
        }

        if (takeEconomy(item, p)) return;

        ItemStack i = new ItemStack(shopItem.getItem());
        ItemMeta meta = i.getItemMeta();
        meta.setLore(new ArrayList<>());
        i.setItemMeta(meta);

        String[] args = item.getCommand().split(" ");
        try {
            i.setAmount(Integer.parseInt(args[1]));
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            i.setAmount(1);
        }
        if (args.length > 2) {
            for (int j = 2; j < args.length; j++) {
                String[] split = args[j].split(":");
                if (split.length == 2) {
                    try {

                        Enchantment ench;
                        try {
                            ench = Enchantment.getByKey(NamespacedKey.minecraft(split[0]));
                        } catch (NoClassDefFoundError e) {
                            ench = Enchantment.getByName(UMaterial.toSpigotEnchantName(split[0]));
                        }
                        int level = Integer.parseInt(split[1]);
                        if (ench != null && level > 0) i.addUnsafeEnchantment(ench, level);
                    } catch (NumberFormatException ignored) {
                    }
                }
            }
        }
        p.getInventory().addItem(i);
        StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseSuccessful(shopItem.getDisplayName()));

        plugin.getPlayerDataStorage().removeItem(p, item);
    }),
    PURCHASE_COMMAND("#purchase-command", (p, category, item) -> {
        ShopItemConfiguration shopItem = null;
        for (ShopItemConfiguration c : category.getShopItems()) {
            if (c.getName().equals(item.getName())) {
                shopItem = c;
                break;
            }
        }

        if (shopItem == null) return;

        Main plugin = Main.getPlugin(Main.class);
        if (p.getInventory().getSize() == 36) {
            StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseNotEnoughSpace());
            return;
        }

        if (takeEconomy(item, p)) return;

        String command = item.getCommand().replace("#purchase-command ", "");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        StringFormatter.sendChatMessage(p, plugin.getMessages().getPurchaseSuccessful(shopItem.getDisplayName()));

        plugin.getPlayerDataStorage().removeItem(p, item);
    }),
    REFRESH_CATEGORY("#refresh-category", (p, category, item) -> {
        if (takeRefreshEconomy(p, 1)) return;
        Main plugin = Main.getPlugin(Main.class);
        plugin.getPlayerDataStorage().refreshCategory(p, category.getName());
        StringFormatter.sendChatMessage(p, plugin.getMessages().getRefreshCategorySuccessfully(category.getDisplayName()));
    }),
    REFRESH_ALL("#refresh-all", (p, category, item) -> {
        Main plugin = Main.getPlugin(Main.class);
        if (takeRefreshEconomy(p, plugin.getCategoryStorage().getCategoriesCount())) return;
        plugin.getPlayerDataStorage().refreshAllItems(p);
        StringFormatter.sendChatMessage(p, plugin.getMessages().getRefreshAllSuccessfully());
    }),
    ;
    private final String command;
    private final Callback callback;
    private CategoryConfiguration category;
    private PlayerShopItemConfiguration item;

    MenuCommand(String command, Callback callback) {
        this.command = command;
        this.callback = callback;
    }

    private interface Callback {
        void executeCommand(Player player, CategoryConfiguration category, PlayerShopItemConfiguration item);
    }

    public String getCommand() {
        return command;
    }

    public void executeCommand(Player p) {
        this.callback.executeCommand(p, category, item);
    }

    public void setCategory(CategoryConfiguration category) {
        this.category = category;
    }

    public void setItem(PlayerShopItemConfiguration item) {
        this.item = item;
    }

    private static boolean takeRefreshEconomy(Player player, int categoriesCount) {
        Main plugin = Main.getPlugin(Main.class);
        CurrencyYAMLConfiguration currency = plugin.getConfiguration().getRefreshCurrency();
        try {
            String value = PlaceholderAPI.setPlaceholders(player, currency.getPlaceholder());
            double playerBalance = Double.parseDouble(value);
            double price = plugin.getConfiguration().getRefreshCategoryCost();
            if (playerBalance < price) {
                StringFormatter.sendChatMessage(player, plugin.getMessages().getNotEnoughBalance(currency.getDisplayName()));
                return true;
            }

            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), currency.getTakeCommand()
                    .replace("{player}", player.getName())
                    .replace("{cost}",
                            String.format(currency.getRounded() ? "%.0f" : "%.2f", price * categoriesCount)));
            return false;
        } catch (NumberFormatException e) {
            StringFormatter.sendChatMessage(player, plugin.getMessages().getPurchaseWrong());
            return true;
        }
    }

    private static boolean takeEconomy(PlayerShopItemConfiguration shopItem, Player player) {
        Main plugin = Main.getPlugin(Main.class);

        List<String> commands = new ArrayList<>();
        for (PlayerShopItemPriceConfiguration price : shopItem.getPrice()) {
            CurrencyYAMLConfiguration currency = null;
            for (CurrencyYAMLConfiguration c : plugin.getConfiguration().getCurrencies()) {
                if (c.getName().equals(price.getCurrency())) {
                    currency = c;
                    break;
                }
            }

            if (currency == null) return true;

            try {
                String value = PlaceholderAPI.setPlaceholders(player, currency.getPlaceholder());
                double playerBalance = Double.parseDouble(value);
                if (playerBalance < price.getPrice()) {
                    StringFormatter.sendChatMessage(player, plugin.getMessages().getNotEnoughBalance(currency.getDisplayName()));
                    return true;
                }

                commands.add(currency.getTakeCommand()
                        .replace("{player}", player.getName())
                        .replace("{cost}",
                                String.format(currency.getRounded() ? "%.0f" : "%.2f", price.getPrice()))
                );
            } catch (NumberFormatException e) {
                StringFormatter.sendChatMessage(player, plugin.getMessages().getPurchaseWrong());
                plugin.getPlayerDataStorage().removeItem(player, shopItem);
                return true;
            }
        }
        for (String s : commands)
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s);

        return false;
    }


}
