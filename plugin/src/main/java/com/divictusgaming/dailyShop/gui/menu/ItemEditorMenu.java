package com.divictusgaming.dailyShop.gui.menu;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.enumerate.ParsedItemTags;
import com.divictusgaming.dailyShop.exception.NotInWizardException;
import com.divictusgaming.dailyShop.gui.Menu;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class ItemEditorMenu extends Menu {

    public ItemEditorMenu(Player player) {
        super(Main.getPlugin(Main.class).getConfiguration().getEditMenuTitle());

        refreshInventory(player);
        openInventory(player);
    }

    @Override
    public void refreshInventory(Player player) {
        final Main plugin = Main.getPlugin(Main.class);
        new BukkitRunnable() {
            @Override
            public void run() {
                putItems(player, plugin.getConfiguration().getEditMenuItems());

                try {
                    putItem(player, plugin.getItemCreationWizard().getItem(player).getItem(), plugin.getConfiguration().getEditItemFinalPosition());
                } catch (NotInWizardException ignored) {
                    StringFormatter.sendChatMessage(player, plugin.getMessages().getNotInWizard());
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    @Override
    public void handleClickEvent(InventoryClickEvent event, ParsedItemTags tags) {
        super.handleClickEvent(event, tags);
    }
}
