package com.divictusgaming.dailyShop.gui.event;

import com.divictusgaming.dailyShop.enumerate.ParsedItemTags;
import com.divictusgaming.dailyShop.gui.Menu;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class InventoryClickEvent implements Listener {
    @EventHandler
    public void onInventoryClick(org.bukkit.event.inventory.InventoryClickEvent event) {
        ItemStack clickedItem = event.getCurrentItem();

        if (clickedItem == null || clickedItem.getType() == Material.AIR) {
            clickedItem = event.getCursor();
            if (clickedItem == null || clickedItem.getType() == Material.AIR) return;
        }

        ParsedItemTags tags = ParsedItemTags.getItemTags(clickedItem);
        if (tags == null) return;

        if (event.getView().getTopInventory().getHolder() instanceof Menu) {
            ((Menu) event.getView().getTopInventory().getHolder()).handleClickEvent(event, tags);
            return;
        }

        if (tags.getFromMenu() == Boolean.TRUE) {
            event.getWhoClicked().setItemOnCursor(null);
            event.setCurrentItem(null);
        }
    }

}
