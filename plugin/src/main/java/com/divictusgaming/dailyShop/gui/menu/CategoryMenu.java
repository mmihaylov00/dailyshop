package com.divictusgaming.dailyShop.gui.menu;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.database.configuration.PlayerConfiguration;
import com.divictusgaming.dailyShop.database.configuration.PlayerShopItemConfiguration;
import com.divictusgaming.dailyShop.database.configuration.ShopItemConfiguration;
import com.divictusgaming.dailyShop.gui.Menu;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class CategoryMenu extends Menu {
    private CategoryConfiguration category;
    private HashMap<Integer, PlayerShopItemConfiguration> items;

    public CategoryMenu(Player player, CategoryConfiguration category) {
        super(Main.getPlugin(Main.class).getConfiguration().getCategoryMenuTitle(category.getDisplayName()));
        this.category = category;
        this.items = new HashMap<>();

        refreshInventory(player);
        openInventory(player);
    }

    @Override
    public void refreshInventory(Player player) {
        final Main plugin = Main.getPlugin(Main.class);
        new BukkitRunnable() {
            @Override
            public void run() {
                putItems(player, plugin.getConfiguration().getCategoryMenuItems());

                PlayerConfiguration playerData = plugin.getPlayerDataStorage().getPlayerData(player.getUniqueId());
                for (PlayerShopItemConfiguration item : playerData.getItems()) {
                    items.put(item.getSlot(), item);

                    for (ShopItemConfiguration c : category.getShopItems())
                        if (c.getName().equals(item.getName())) {
                            putItem(player, c.getItem(), item.getSlot(), item.getPlaceholders());
                            break;
                        }

                }
            }
        }.runTaskAsynchronously(plugin);
    }

    public CategoryConfiguration getCategory() {
        return category;
    }

    public HashMap<Integer, PlayerShopItemConfiguration> getItems() {
        return items;
    }
}
