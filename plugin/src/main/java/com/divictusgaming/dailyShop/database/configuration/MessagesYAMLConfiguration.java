package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPathPrefix;

import java.util.ArrayList;
import java.util.List;

@YAMLPathPrefix(path = "message")
@YAMLFileName(name = "messages.yml")
public class MessagesYAMLConfiguration extends DatabaseConfiguration {

    public MessagesYAMLConfiguration() {
        setupAdminHelp();
        setupCategoryHelp();
        setupItemHelp();
        setupConsoleHelp();
    }

    //the values here are the default ones
    @YAMLPath(path = "prefix")
    private String prefix = "&b&lDailyShop &7&l> &r";

    @YAMLPath(path = "admin-help")
    private List<String> adminHelp = new ArrayList<>();

    private void setupAdminHelp() {
        adminHelp.add("&e&l--- &f&lCommands &e&l---");
        adminHelp.add("&c&lHover to see description &7&l| &c&lClick to get a suggestion");
        adminHelp.add("&6/dshop reload[hover:&eReload the configuration files.][suggest:/dshop reload]");
        adminHelp.add("&6/dshop category[hover:&eSee category commands.][suggest:/dshop category]");
        adminHelp.add("&6/dshop item[hover:&eSee item commands.][suggest:/dshop item]");
    }

    @YAMLPath(path = "console-help")
    private List<String> consoleHelp = new ArrayList<>();

    private void setupConsoleHelp() {
        consoleHelp.add("&e&l--- &f&lCommands &e&l---");
        consoleHelp.add("&6/dshop reload &e- Reload the configuration files");
    }

    @YAMLPath(path = "category-help")
    private List<String> categoryHelp = new ArrayList<>();

    private void setupCategoryHelp() {
        categoryHelp.add("&e&l--- &f&lCategory Commands &e&l---");
        categoryHelp.add("&c&lHover to see description &7&l| &c&lClick to get a suggestion");
        categoryHelp.add("&6/dshop category create <name> <slot> <command>[hover:&eCreate a new category.\\n&b<slot> - The slot in the inventory\\n&b<command> - The command that will be used to open the menu][suggest:/dshop category create ]");
        categoryHelp.add("&6/dshop category list[hover:&eList all categories.][suggest:/dshop category list]");
        categoryHelp.add("&6/dshop category edit item <category>[hover:&eChange the category's item to the item in your hand.][suggest:/dshop category edit item ]");
        categoryHelp.add("&6/dshop category edit set-slots <category> <slot> ...[slot][hover:&eSet the category items' slots.][suggest:/dshop category edit set-slots ]");
        categoryHelp.add("&6/dshop category edit slot <category> <slot>[hover:&eChange a category's slot.\\n&b<slot> - The slot in the inventory][suggest:/dshop category edit slot ]");
        categoryHelp.add("&6/dshop category edit command <category> <command>[hover:&eChange a category's command.\\n&b<command> - The command that will be used to open the menu][suggest:/dshop category edit command ]");
        categoryHelp.add("&6/dshop category edit permission <category> <command>[hover:&eChange a category's permission.\\n&b<permission> - The permission that will be needed to open the menu. Set to &nnone&b for no permission][suggest:/dshop category edit permission ]");
        categoryHelp.add("&6/dshop category delete <name>[hover:&eDelete a category.][suggest:/dshop category delete ]");
        categoryHelp.add("&6/dshop category list-items[hover:&eList the category's items.][suggest:/dshop category list-items]");
    }

    @YAMLPath(path = "item-help")
    private List<String> itemHelp = new ArrayList<>();

    private void setupItemHelp() {
        itemHelp.add("&e&l--- &f&lItem Commands &e&l---");
        itemHelp.add("&c&lHover to see description &7&l| &c&lClick to get a suggestion");
        itemHelp.add("&6/dshop item create <name> <category>[hover:&eStart creating an item.][suggest:/dshop item create ]");
        itemHelp.add("&6/dshop item cancel[hover:&eCancel the item creation.][suggest:/dshop item cancel]");
        itemHelp.add("&6/dshop item save[hover:&eSave the item.][suggest:/dshop item save]");
        itemHelp.add("&6/dshop item open[hover:&eOpen the creation GUI.][suggest:/dshop item open]");
        itemHelp.add("&6/dshop item edit <category> <item-name>[hover:&eEdit an item.][suggest:/dshop item edit ]");
        itemHelp.add("&6/dshop item delete <category> <item-name>[hover:&eDelete an item from a category.][suggest:/dshop item delete ]");
        itemHelp.add("&6/dshop item list <conditions|prices|variables>[hover:&eList editing item's values.][suggest:/dshop item list ]");
        itemHelp.add("&6/dshop item set chance <percentage>[hover:&eSet the item's appear chance.\\n&b<percentage> - the chance for the item to appear to a player][suggest:/dshop item set chance ]");
        itemHelp.add("&6/dshop item set command <command>[hover:&eSet the item's appear chance.\\n&b<command> - the command that will be executed when a player purchase the item. It can contain variables from the item's variables in the {variable_name} format][suggest:/dshop set command ]");
        itemHelp.add("&6/dshop item set amount <amount>[hover:&eSet the item's display amount.\\n&b<amount> - the amount for the display item, between 1 and 64][suggest:/dshop item set amount ]");
        itemHelp.add("&6/dshop item set material[hover:&eSet the item's display material to the item in your hand.][suggest:/dshop item set material]");
        itemHelp.add("&6/dshop item set display-name <name> [hover:&eSet the item's display name.][suggest:/dshop item set display-name ]");
        itemHelp.add("&6/dshop item set hide-tags <true|false> [hover:&eSet the hidden status of the item's tags (enchants, damage, etc).][suggest:/dshop item set hide-tags ]");
        itemHelp.add("&6/dshop item add condition <placeholder> <min> <max>[hover:&eAdd a condition for when to sell the item to the player.\\n&b<placeholder> - A PlaceholderAPI placeholder that will be checked.][suggest:/dshop item add condition ]");
        itemHelp.add("&6/dshop item add price <currency(min-max)> ...[currency(min-max)][hover:&eAdd a price for the item.\\n&b<currency(min-max)> - Add a currency to the item, currency comes from the config, example: coins(0.5-10) for the price to be between 0.5 and 10 coins][suggest:/dshop item add price ]");
        itemHelp.add("&6/dshop item add variable <variable> <min> <max>[hover:&eAdd a variable for the command that will be executed.\\n&b<variable> - Use {<variable>} in the command][suggest:/dshop item add variable ]");
        itemHelp.add("&6/dshop item remove <condition|price|variable> <num>[hover:&eRemove a variable, condition or a price from the currently edited item.][suggest:/dshop item remove ]");
    }

    @YAMLPath(path = "unknown-command")
    private String unknownCommand = "&cUnknown command!";

    @YAMLPath(path = "reload-started")
    private String reloadStarted = "&aReload started!";

    @YAMLPath(path = "reload-completed")
    private String reloadCompleted = "&aReload completed!";

    @YAMLPath(path = "help.error.empty-hand")
    private String emptyHandError = "&cPlease hold an item in your hand.";

    @YAMLPath(path = "help.error.no-permission")
    private String noPermissionError = "&cYou don't have access to this command!";

    @YAMLPath(path = "help.error.category-not-found")
    private String categoryNotFoundError = "&cCategory not found!";

    @YAMLPath(path = "help.error.item-not-found")
    private String itemNotFoundError = "&cItem not found!";

    @YAMLPath(path = "help.error.invalid-slot")
    private String categoryInvalidSlot = "&cPlease choose a valid menu slot.";

    @YAMLPath(path = "help.category.create.usage")
    private String createCategoryUsage = "&cUsage: &f/dshop category create <name> <position> <command>[hover:&eClick to get a suggestion.][suggest:/dshop category create ]";

    @YAMLPath(path = "help.category.create.already-exists")
    private String createCategoryAlreadyExists = "&cCategory already exists.";

    @YAMLPath(path = "help.category.create.created")
    private String createCategoryCreated = "&aCategory created!";

    @YAMLPath(path = "help.category.delete.usage")
    private String deleteCategoryUsage = "&cUsage: &f/dshop category delete <name>[hover:&eClick to get a suggestion.][suggest:/dshop category delete ]";

    @YAMLPath(path = "help.category.delete.deleted")
    private String deleteCategoryDeleted = "&aCategory deleted!";

    @YAMLPath(path = "help.category.edit.updated")
    private String editCategoryUpdated = "&aCategory updated!";

    @YAMLPath(path = "help.category.edit.command-registered")
    private String editCategoryCommandRegistered = "&cThis command has already been register!";

    @YAMLPath(path = "help.category.list")
    private String listCategory = "&aCategories: &e{categories}";

    @YAMLPath(path = "help.category.list-items.listed")
    private String listCategoryItems = "&aCategory {category} items: &e{items}";

    @YAMLPath(path = "help.category.list-items.usage")
    private String listCategoryItemsUsage = "&cUsage: &f/dshop category list-items <category>[hover:&eClick to get a suggestion.][suggest:/dshop category list-items ]";

    @YAMLPath(path = "help.item.create.usage")
    private String createItemUsage = "&cUsage: &f/dshop item create <name> <category>[hover:&eClick to get a suggestion.][suggest:/dshop item create ]";

    @YAMLPath(path = "help.item.create.started")
    private String createItemStarted = "&aItem creation started!";

    @YAMLPath(path = "help.item.not-creating")
    private String notInWizard = "&cYou are not creating an item!";

    @YAMLPath(path = "help.item.already-creating")
    private String alreadyInWizard = "&cYou are already creating an item, use &7/dshop item open&c to continue editing it![hover:&eClick to get a suggestion.][suggest:/dshop item open]";

    @YAMLPath(path = "help.item.name-exists")
    private String itemExists = "&cItem with that name already exists!";

    @YAMLPath(path = "help.item.saved")
    private String itemSaved = "&aItem saved!";

    @YAMLPath(path = "help.item.missing-configuration")
    private String itemMissingSetting = "&cThe item is missing a {setting} setting!";

    @YAMLPath(path = "help.item.need-at-least-one")
    private String itemNeedAtLeastOne = "&cThe item need at least one {setting} setting!";

    @YAMLPath(path = "help.item.command.set")
    private String itemCommandSet = "&aItem command set!";

    @YAMLPath(path = "help.item.command.usage")
    private String setItemCommandUsage = "&cUsage: &f/dshop item set command <command>[hover:&eClick to get a suggestion.][suggest:/dshop item set command ]";

    @YAMLPath(path = "help.item.chance.negative-or-zero")
    private String itemChanceNegativeOrZero = "&cItem chance can't be negative or zero!";

    @YAMLPath(path = "help.item.chance.set")
    private String itemChanceSet = "&aItem chance set!";

    @YAMLPath(path = "help.item.chance.usage")
    private String setItemChanceUsage = "&cUsage: &f/dshop item set chance <percentage>[hover:&eClick to get a suggestion.][suggest:/dshop item set chance ]";

    @YAMLPath(path = "help.item.display-name.set")
    private String itemDisplayNameSet = "&aItem display name set!";

    @YAMLPath(path = "help.item.display-name.usage")
    private String setItemDisplayNameUsage = "&cUsage: &f/dshop item set display-name <name>[hover:&eClick to get a suggestion.][suggest:/dshop item set display-name ]";

    @YAMLPath(path = "help.item.material.set")
    private String itemMaterialSet = "&aItem material set!";

    @YAMLPath(path = "help.item.material.usage")
    private String setItemMaterialUsage = "&cUsage: &f/dshop item set material <name>[hover:&eClick to get a suggestion.][suggest:/dshop item set material ]";

    @YAMLPath(path = "help.item.hide-tags.set")
    private String itemHideTagsSet = "&aItem hide tags changed!";

    @YAMLPath(path = "help.item.hide-tags.usage")
    private String setItemTagsUsage = "&cUsage: &f/dshop item set hide-tags <true|false>[hover:&eClick to get a suggestion.][suggest:/dshop item set hide-tags ]";

    @YAMLPath(path = "help.item.amount.set")
    private String itemAmountSet = "&aItem amount set!";

    @YAMLPath(path = "help.item.amount.usage")
    private String setAmountUsage = "&cUsage: &f/dshop item set amount <num>[hover:&eClick to get a suggestion.][suggest:/dshop item set amount ]";

    @YAMLPath(path = "help.item.price.added")
    private String itemPriceAdded = "&aItem price added!";

    @YAMLPath(path = "help.item.price.usage")
    private String itemPriceUsage = "&cUsage: &f/dshop item add price <currency(min-max)> ...[currency(min-max)][hover:&eClick to get a suggestion.\\n&b<currency(min-max)> example - coins(5, 15.5) - between 5 and 15.5 coins][suggest:/dshop item add price ]";

    @YAMLPath(path = "help.item.price.duplicated-currencies")
    private String itemPriceDuplicated = "&cDuplicated currency {currency} for the same item!";

    @YAMLPath(path = "help.item.price.unknown-currency")
    private String itemPriceUnknownCurrency = "&cUnknown currency {currency}!";

    @YAMLPath(path = "help.item.variable.added")
    private String itemVariableAdded = "&aItem variable added!";

    @YAMLPath(path = "help.item.variable.usage")
    private String itemVariableUsage = "&cUsage: &f/dshop item add variable <variable> <min> <max>[hover:&eClick to get a suggestion.][suggest:/dshop item add variable ]";

    @YAMLPath(path = "help.item.variable.exists")
    private String itemVariableExists = "&cVariable with that name already exists!";

    @YAMLPath(path = "help.item.condition.added")
    private String itemConditionAdded = "&aItem condition added!";

    @YAMLPath(path = "help.item.condition.usage")
    private String itemConditionUsage = "&cUsage: &f/dshop item add condition <placeholder> <min> <max>[hover:&eClick to get a suggestion.][suggest:/dshop item add condition ]";

    @YAMLPath(path = "help.item.condition.exists")
    private String itemConditionExists = "&cCondition with that placeholder already exists!";

    @YAMLPath(path = "help.item.lore.added")
    private String itemLoreAdded = "&aItem lore line added!";

    @YAMLPath(path = "help.item.lore.usage")
    private String itemLoreUsage = "&cUsage: &f/dshop item add lore <lore>[hover:&eClick to get a suggestion.][suggest:/dshop item add lore ]";

    @YAMLPath(path = "help.item.delete-item.usage")
    private String itemDeleteUsage = "&cUsage: &f/dshop item delete <category> <item-name>[hover:&eClick to get a suggestion.][suggest:/dshop item delete ]";

    @YAMLPath(path = "help.item.delete-item.successful")
    private String itemRemoveSuccessful = "&cItem deleted!";

    @YAMLPath(path = "help.item.min-more-than-max")
    private String itemValueMinMoreThanMax = "&cMin value is more than max!";

    @YAMLPath(path = "help.item.placeholder-not-number")
    private String itemPlaceholderNaN = "&cThe placeholder must be a number!";

    @YAMLPath(path = "help.item.canceled")
    private String itemCanceled = "&aItem creation canceled!";

    @YAMLPath(path = "help.item.remove-parameter.usage")
    private String itemRemoveParamUsage = "&cUsage: &f/dshop item remove <condition|price|variable> <num>[hover:&eClick to get a suggestion.][suggest:/dshop item remove ]";

    @YAMLPath(path = "help.item.remove-parameter.invalid-num")
    private String itemRemoveParamInvalidNum = "&cInvalid parameter number!";

    @YAMLPath(path = "help.item.remove-parameter.condition-removed")
    private String itemRemoveParamConditionRemoved = "&aCondition removed!";

    @YAMLPath(path = "help.item.remove-parameter.variable-removed")
    private String itemRemoveParamVariableRemoved = "&aVariable removed!";

    @YAMLPath(path = "help.item.remove-parameter.lore-line-removed")
    private String itemRemoveParamLoreLineRemoved = "&aLore line removed!";

    @YAMLPath(path = "help.item.remove-parameter.condition-removed")
    private String itemRemoveParamPriceRemoved = "&aPrice removed!";

    @YAMLPath(path = "help.item.list.usage")
    private String itemListUsage = "&cUsage: &f/dshop item list <condition|price|variable>[hover:&eClick to get a suggestion.][suggest:/dshop item list ]";

    @YAMLPath(path = "help.item.list.conditions.title")
    private String itemListConditionsTitle = "&a&lNumber &7&l| &e&lCondition";

    @YAMLPath(path = "help.item.list.conditions.row")
    private String itemListConditionsRow = "&a{num} &7| &e{condition}[hover:&eClick to remove.][suggest:/dshop item remove condition {num}]";

    @YAMLPath(path = "help.item.list.prices.title")
    private String itemListPricesTitle = "&a&lNumber &7&l| &e&lPrice";

    @YAMLPath(path = "help.item.list.prices.row")
    private String itemListPricesRow = "&a{num} &7| &e{price}[hover:&eClick to remove.][suggest:/dshop item remove price {num}]";

    @YAMLPath(path = "help.item.list.lore.title")
    private String itemListLoreTitle = "&a&lNumber &7&l| &e&lLore";

    @YAMLPath(path = "help.item.list.lore.row")
    private String itemListLoreRow = "&a{num} &7| &r{lore}[hover:&eClick to remove.][suggest:/dshop item remove lore {num}]";

    @YAMLPath(path = "help.item.list.variables.title")
    private String itemListVariablesTitle = "&a&lNumber &7&l| &e&lVariable";

    @YAMLPath(path = "help.item.list.variables.row")
    private String itemListVariablesRow = "&a{num} &7| &e{variable}[hover:&eClick to remove.][suggest:/dshop item remove variable {num}]";

    @YAMLPath(path = "help.purchase.not-enough-balance")
    private String purchaseNotEnoughBalance = "&cYou don't have enough {currency}&c!";

    @YAMLPath(path = "help.purchase.successful")
    private String purchaseSuccessful = "&aYou have successfully purchased {item}&a!";

    @YAMLPath(path = "help.purchase.something-went-wrong")
    private String purchaseWrong = "&cThere was a problem with your purchase, no money were taken from your account!";

    @YAMLPath(path = "help.purchase.not-enough-space")
    private String purchaseNotEnoughSpace = "&cYou don't have enough space in your inventory!";

    @YAMLPath(path = "help.refresh.category-successful")
    private String refreshCategorySuccessfully = "&aCategory {category}&a has been refreshed!";

    @YAMLPath(path = "help.refresh.all-successful")
    private String refreshAllSuccessfully = "&aAll categories have been refreshed!";

    @YAMLPath(path = "format.not-set")
    private String notSet = "&cnot set";

    @YAMLPath(path = "format.time.hour")
    private String hour = " Hour ";

    @YAMLPath(path = "format.time.hours")
    private String hours = " Hours ";

    @YAMLPath(path = "format.time.minute")
    private String minute = " Minute ";

    @YAMLPath(path = "format.time.minutes")
    private String minutes = " Minutes ";

    @YAMLPath(path = "format.time.second")
    private String second = " Second ";

    @YAMLPath(path = "format.time.seconds")
    private String seconds = " Seconds ";

    public List<String> getAdminHelp() {
        return adminHelp;
    }

    public List<String> getCategoryHelp() {
        return categoryHelp;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getUnknownCommand() {
        return prefix + unknownCommand;
    }

    public String getNoPermissionError() {
        return prefix + noPermissionError;
    }

    public String getCreateCategoryUsage() {
        return prefix + createCategoryUsage;
    }

    public String getEmptyHandError() {
        return prefix + emptyHandError;
    }

    public String getCategoryInvalidSlot() {
        return prefix + categoryInvalidSlot;
    }

    public String getCreateCategoryAlreadyExists() {
        return prefix + createCategoryAlreadyExists;
    }

    public String getCreateCategoryCreated() {
        return prefix + createCategoryCreated;
    }

    public String getReloadStarted() {
        return prefix + reloadStarted;
    }

    public String getReloadCompleted() {
        return prefix + reloadCompleted;
    }

    public String getDeleteCategoryUsage() {
        return prefix + deleteCategoryUsage;
    }

    public String getCategoryNotFoundError() {
        return prefix + categoryNotFoundError;
    }

    public String getDeleteCategoryDeleted() {
        return prefix + deleteCategoryDeleted;
    }

    public String getEditCategoryUpdated() {
        return prefix + editCategoryUpdated;
    }

    public String getListCategory(String categories) {
        return prefix + listCategory.replace("{categories}", categories);
    }

    public String getListCategoryItems(String category, String items) {
        return prefix + listCategoryItems.replace("{category}", category).replace("{items}", items);
    }

    public String getEditCategoryCommandRegistered() {
        return prefix + editCategoryCommandRegistered;
    }

    public String getCreateItemUsage() {
        return prefix + createItemUsage;
    }

    public String getHour() {
        return hour;
    }

    public String getHours() {
        return hours;
    }

    public String getMinute() {
        return minute;
    }

    public String getMinutes() {
        return minutes;
    }

    public String getSecond() {
        return second;
    }

    public String getSeconds() {
        return seconds;
    }

    public List<String> getItemHelp() {
        return itemHelp;
    }

    public String getCreateItemStarted() {
        return prefix + createItemStarted;
    }

    public String getNotInWizard() {
        return prefix + notInWizard;
    }

    public String getItemSaved() {
        return prefix + itemSaved;
    }

    public String getItemExists() {
        return prefix + itemExists;
    }

    public String getItemCanceled() {
        return prefix + itemCanceled;
    }

    public String getAlreadyInWizard() {
        return prefix + alreadyInWizard;
    }

    public List<String> getConsoleHelp() {
        return consoleHelp;
    }

    public String getNotSet() {
        return notSet;
    }

    public String getSetItemChanceUsage() {
        return prefix + setItemChanceUsage;
    }

    public String getItemCommandSet() {
        return prefix + itemCommandSet;
    }

    public String getItemChanceSet() {
        return prefix + itemChanceSet;
    }

    public String getSetItemCommandUsage() {
        return prefix + setItemCommandUsage;
    }

    public String getItemPriceAdded() {
        return prefix + itemPriceAdded;
    }

    public String getItemPriceUsage() {
        return prefix + itemPriceUsage;
    }

    public String getItemVariableAdded() {
        return prefix + itemVariableAdded;
    }

    public String getItemVariableUsage() {
        return prefix + itemVariableUsage;
    }

    public String getItemPriceDuplicated(String currency) {
        return prefix + itemPriceDuplicated.replace("{currency}", currency);
    }

    public String getItemVariableExists() {
        return prefix + itemVariableExists;
    }

    public String getUnknownCurrency(String currency) {
        return prefix + itemPriceUnknownCurrency.replace("{currency}", currency);
    }

    public String getItemValueMinMoreThanMax() {
        return prefix + itemValueMinMoreThanMax;
    }

    public String getItemConditionAdded() {
        return prefix + itemConditionAdded;
    }

    public String getItemConditionUsage() {
        return prefix + itemConditionUsage;
    }

    public String getItemConditionExists() {
        return prefix + itemConditionExists;
    }

    public String getItemMissingSetting(String setting) {
        return prefix + itemMissingSetting.replace("{setting}", setting);
    }

    public String getItemNeedAtLeastOne(String setting) {
        return prefix + itemNeedAtLeastOne.replace("{setting}", setting);
    }

    public String getItemChanceNegativeOrZero() {
        return prefix + itemChanceNegativeOrZero;
    }

    public String getItemDeleteUsage() {
        return prefix + itemDeleteUsage;
    }

    public String getItemRemoveSuccessful() {
        return prefix + itemRemoveSuccessful;
    }

    public String getItemNotFoundError() {
        return prefix + itemNotFoundError;
    }

    public String getItemRemoveParamUsage() {
        return prefix + itemRemoveParamUsage;
    }

    public String getItemRemoveParamInvalidNum() {
        return prefix + itemRemoveParamInvalidNum;
    }

    public String getItemRemoveParamConditionRemoved() {
        return prefix + itemRemoveParamConditionRemoved;
    }

    public String getItemRemoveParamVariableRemoved() {
        return prefix + itemRemoveParamVariableRemoved;
    }

    public String getItemRemoveParamLoreLineRemoved() {
        return itemRemoveParamLoreLineRemoved;
    }

    public String getItemRemoveParamPriceRemoved() {
        return prefix + itemRemoveParamPriceRemoved;
    }

    public String getItemListUsage() {
        return itemListUsage;
    }

    public String getListCategoryItemsUsage() {
        return prefix + listCategoryItemsUsage;
    }

    public String getItemListConditionsTitle() {
        return itemListConditionsTitle;
    }

    public String getItemListConditionsRow(String condition, int num) {
        return itemListConditionsRow.replaceAll("\\{num}", num + "").replace("{condition}", condition);
    }

    public String getItemListPricesTitle() {
        return itemListPricesTitle;
    }

    public String getItemListPricesRow(String price, int num) {
        return itemListPricesRow.replaceAll("\\{num}", num + "").replace("{price}", price);
    }

    public String getItemListLoreTitle() {
        return itemListLoreTitle;
    }

    public String getItemListLoreRow(String lore, int num) {
        return itemListLoreRow.replaceAll("\\{num}", num + "").replace("{lore}", lore);
    }

    public String getItemListVariablesTitle() {
        return itemListVariablesTitle;
    }

    public String getItemListVariablesRow(String variable, int num) {
        return itemListVariablesRow.replaceAll("\\{num}", num + "").replace("{variable}", variable);
    }

    public String getItemPlaceholderNaN() {
        return prefix + itemPlaceholderNaN;
    }

    public String getNotEnoughBalance(String currency) {
        return prefix + purchaseNotEnoughBalance.replace("{currency}", currency);
    }

    public String getPurchaseSuccessful(String item) {
        return prefix + purchaseSuccessful.replace("{item}", item);
    }

    public String getPurchaseWrong() {
        return prefix + purchaseWrong;
    }

    public String getPurchaseNotEnoughSpace() {
        return prefix + purchaseNotEnoughSpace;
    }

    public String getRefreshCategorySuccessfully(String category) {
        return prefix + refreshCategorySuccessfully.replace("{category}", category);
    }

    public String getRefreshAllSuccessfully() {
        return prefix + refreshAllSuccessfully;
    }

    public String getItemDisplayNameSet() {
        return prefix + itemDisplayNameSet;
    }

    public String getSetItemDisplayNameUsage() {
        return prefix + setItemDisplayNameUsage;
    }

    public String getItemMaterialSet() {
        return prefix + itemMaterialSet;
    }

    public String getSetItemMaterialUsage() {
        return prefix + setItemMaterialUsage;
    }

    public String getItemHideTagsSet() {
        return prefix + itemHideTagsSet;
    }

    public String getSetItemTagsUsage() {
        return prefix + setItemTagsUsage;
    }

    public String getItemAmountSet() {
        return prefix + itemAmountSet;
    }

    public String getSetAmountUsage() {
        return prefix + setAmountUsage;
    }

    public String getItemLoreAdded() {
        return prefix + itemLoreAdded;
    }

    public String getItemLoreUsage() {
        return prefix + itemLoreUsage;
    }
}
