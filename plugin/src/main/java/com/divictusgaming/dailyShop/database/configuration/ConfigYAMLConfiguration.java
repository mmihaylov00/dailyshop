package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.CurrencyYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.DisplayItemConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLObject;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPathPrefix;
import com.divictusgaming.dailyShop.gui.command.MenuCommand;
import com.divictusgaming.dailyShop.util.UMaterial;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@YAMLPathPrefix(path = "config")
@YAMLFileName(name = "config.yml")
public class ConfigYAMLConfiguration extends DatabaseConfiguration {
    @YAMLPath(path = "database-type")
    private String databaseType = "file";

    @YAMLPath(path = "mysql.url")
    private String mysqlUrl = "jdbc:mysql://localhost:3306/databasename?createDatabaseIfNotExist=true";

    @YAMLPath(path = "mysql.username")
    private String mysqlUsername = "root";

    @YAMLPath(path = "mysql.password")
    private String mysqlPassword = "pass";

    @YAMLPath(path = "refresh.timer-every-minutes")
    private int refreshAfter = 1440;

    @YAMLPath(path = "refresh.cost")
    private double refreshCategoryCost = 5;

    @YAMLPath(path = "refresh.currency")
    private String refreshCurrency = "tokens";

    @YAMLObject(CurrencyYAMLConfiguration.class)
    @YAMLPath(path = "currencies")
    private List<CurrencyYAMLConfiguration> currencies;

    @YAMLPath(path = "menu.rows")
    private int menuRows = 5;

    @YAMLPath(path = "menu.fill-empty")
    private boolean fillEmpty = true;

    @YAMLObject(DisplayItemConfiguration.class)
    @YAMLPath(path = "menu.fill-item")
    private DisplayItemConfiguration fillItem;

    @YAMLPath(path = "menu.main.title")
    private String mainMenuTitle = "&9&lDaily Shop > Categories";

    @YAMLObject(DisplayItemConfiguration.class)
    @YAMLPath(path = "menu.main.items")
    private List<DisplayItemConfiguration> mainMenuItems;

    @YAMLPath(path = "menu.category.title")
    private String categoryMenuTitle = "&9&lDaily Shop > {category_name}";

    @YAMLObject(DisplayItemConfiguration.class)
    @YAMLPath(path = "menu.category.items")
    private List<DisplayItemConfiguration> categoryMenuItems;

    @YAMLPath(path = "menu.edit-item.title")
    private String editMenuTitle = "&9&lDaily Shop > Edit Item";

    @YAMLObject(DisplayItemConfiguration.class)
    @YAMLPath(path = "menu.edit-item.items")
    private List<DisplayItemConfiguration> editMenuItems;

    @YAMLPath(path = "menu.edit-item.final-item-positon")
    private int editItemFinalPosition = 40;

    public ConfigYAMLConfiguration() {
        setupCurrencies();
        setupMainMenuFillItem();
        setupMainMenuItems();
        setupCategoryMenuItems();
        setupItemEditMenuItems();
    }

    private void setupCurrencies() {
        currencies = new ArrayList<>();
        currencies.add(new CurrencyYAMLConfiguration("coins", "&e&lCoins",
                "%vault_eco_balance_fixed%", "eco take {player} {cost}", false));
        currencies.add(new CurrencyYAMLConfiguration("tokens", "&9&lTokens",
                "%mysqltokens_tokens%", "mysqltokens remove {player} {cost}", true));
    }

    private void setupMainMenuItems() {
        mainMenuItems = new ArrayList<>();

        DisplayItemConfiguration close = new DisplayItemConfiguration(new ItemStack(Material.BARRIER, 1), 36, MenuCommand.CLOSE.getCommand());
        close.setDisplayName("&4&lClose");
        mainMenuItems.add(close);

        DisplayItemConfiguration refreshAll = new DisplayItemConfiguration(new ItemStack(UMaterial.ENDER_EYE.getMaterial(), 1), 20, MenuCommand.REFRESH_ALL.getCommand());
        refreshAll.setDisplayName("&6&lRefresh All");

        refreshAll.setLore(Arrays.stream(new String[]{
                "&9&lAuto Refresh in: &7{refresh_time}",
                "&aClick to refresh all daily shops",
                "&b",
                "&aCost: &7{refresh_cost}"
        }).collect(Collectors.toList()));

        mainMenuItems.add(refreshAll);
    }

    private void setupCategoryMenuItems() {
        categoryMenuItems = new ArrayList<>();
        DisplayItemConfiguration main = new DisplayItemConfiguration(new ItemStack(Material.ARROW, 1), 36, "/dshop");
        main.setDisplayName("&e&lMain Menu");
        categoryMenuItems.add(main);

        DisplayItemConfiguration refreshAll = new DisplayItemConfiguration(new ItemStack(UMaterial.ENDER_EYE.getMaterial(), 1), 20, MenuCommand.REFRESH_CATEGORY.getCommand());
        refreshAll.setDisplayName("&6&lRefresh");

        refreshAll.setLore(Arrays.stream(new String[]{
                "&9&lAuto Refresh in: &7{refresh_time}",
                "&aClick to refresh this category",
                "&b",
                "&aCost: &7{refresh_cost_category}"
        }).collect(Collectors.toList()));

        categoryMenuItems.add(refreshAll);
    }

    private void setupItemEditMenuItems() {
        DisplayItemConfiguration chance = new DisplayItemConfiguration(new ItemStack(Material.SLIME_BALL, 1),
                28, "/dshop item set chance");
        chance.setDisplayName("&eSet Chance");
        chance.setLore(Arrays.stream(new String[]{
                "&7Current chance:",
                "&b{edit_chance}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration displayName = new DisplayItemConfiguration(new ItemStack(Material.ITEM_FRAME, 1),
                29, "/dshop item set display-name");
        displayName.setDisplayName("&eSet Display Name");

        DisplayItemConfiguration amount = new DisplayItemConfiguration(new ItemStack(UMaterial.WHEAT_SEEDS.getMaterial(), 1),
                30, "/dshop item set amount");
        amount.setDisplayName("&eSet Amount");

        DisplayItemConfiguration hideTags = new DisplayItemConfiguration(new ItemStack(Material.NAME_TAG, 1),
                32, "/dshop item set hide-tags");
        hideTags.setDisplayName("&eToggle Hide Tags");
        hideTags.setLore(Arrays.stream(new String[]{
                "&7Hide enchants and item information",
                "&bEnabled: {edit_hide_tags}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration material = new DisplayItemConfiguration(new ItemStack(Material.GLASS, 1),
                33, "/dshop item set material");
        material.setDisplayName("&eSet Material");
        material.setLore(Arrays.stream(new String[]{
                "&7To the item in your hand"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration command = new DisplayItemConfiguration(new ItemStack(UMaterial.COMMAND_BLOCK_MINECART.getMaterial(), 1),
                34, "/dshop item set command");
        command.setDisplayName("&eSet Command");
        command.setLore(Arrays.stream(new String[]{
                "&7Current command:",
                "&b{edit_command}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration listConditions = new DisplayItemConfiguration(new ItemStack(Material.BOOK, 1),
                10, "/dshop item list conditions");
        listConditions.setDisplayName("&eList Conditions");
        listConditions.setLore(Arrays.stream(new String[]{
                "&7Current Conditions:",
                "&b{edit_conditions}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration listPrices = new DisplayItemConfiguration(new ItemStack(Material.BOOK, 1),
                12, "/dshop item list prices");
        listPrices.setDisplayName("&eList Prices");
        listPrices.setLore(Arrays.stream(new String[]{
                "&7Current Prices:",
                "&b{edit_prices}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration listVariables = new DisplayItemConfiguration(new ItemStack(Material.BOOK, 1),
                14, "/dshop item list variables");
        listVariables.setDisplayName("&eList Variables");
        listVariables.setLore(Arrays.stream(new String[]{
                "&7Current Variables:",
                "&b{edit_variables}"
        }).collect(Collectors.toList()));

        DisplayItemConfiguration listLore = new DisplayItemConfiguration(new ItemStack(Material.BOOK, 1),
                16, "/dshop item list lore");
        listLore.setDisplayName("&eList Lore");
        listLore.setLore(Arrays.stream(new String[]{
                "&7Current Lore:",
                "&b{edit_lore}"
        }).collect(Collectors.toList()));


        DisplayItemConfiguration addCondition = new DisplayItemConfiguration(new ItemStack(Material.NETHER_STAR, 1),
                1, "/dshop item add condition");
        addCondition.setDisplayName("&eAdd Condition");
        addCondition.setLore(Arrays.stream(new String[]{
                "&7Used to determine when and",
                "&7to who to show the item to."
        }).collect(Collectors.toList()));

        DisplayItemConfiguration addPrice = new DisplayItemConfiguration(new ItemStack(Material.NETHER_STAR, 1),
                3, "/dshop item add price");
        addPrice.setDisplayName("&eAdd Price");
        addPrice.setLore(Arrays.stream(new String[]{
                "&7Used to determine what",
                "&7the item's price will be."
        }).collect(Collectors.toList()));

        DisplayItemConfiguration addVariable = new DisplayItemConfiguration(new ItemStack(Material.NETHER_STAR, 1),
                5, "/dshop item add variable");
        addVariable.setDisplayName("&eAdd Variable");
        addVariable.setLore(Arrays.stream(new String[]{
                "&7Used in the command as {variable},",
                "&7to set the item's stats."
        }).collect(Collectors.toList()));

        DisplayItemConfiguration addLore = new DisplayItemConfiguration(new ItemStack(Material.NETHER_STAR, 1),
                7, "/dshop item add lore");
        addLore.setDisplayName("&eAdd Lore Line");
        addLore.setLore(Arrays.stream(new String[]{
                "&7Add a new lore line to the item.",
                "&7Can contain {variable}.",
                "&7Use {if:variable} to hide",
                "&7the row if the variable is 0."
        }).collect(Collectors.toList()));

        ItemStack saveItem;
        try {
            saveItem = new ItemStack(UMaterial.LIME_STAINED_GLASS_PANE.getMaterial(), 1, (short) 0, UMaterial.LIME_STAINED_GLASS_PANE.getData());
        }catch (IllegalArgumentException e){
            saveItem = new ItemStack(UMaterial.LIME_STAINED_GLASS_PANE.getMaterial(), 1);
        }

        DisplayItemConfiguration save = new DisplayItemConfiguration(saveItem, 42, "/dshop item save");
        save.setDisplayName("&aSave Item");

        DisplayItemConfiguration save2 = new DisplayItemConfiguration(saveItem, 43, "/dshop item save");
        save2.setDisplayName("&aSave Item");

        DisplayItemConfiguration save3 = new DisplayItemConfiguration(saveItem, 44, "/dshop item save");
        save3.setDisplayName("&aSave Item");

        ItemStack cancelItem;
        try {
            cancelItem = new ItemStack(UMaterial.RED_STAINED_GLASS_PANE.getMaterial(), 1, (short) 0, UMaterial.RED_STAINED_GLASS_PANE.getData());
        }catch (IllegalArgumentException e){
            cancelItem = new ItemStack(UMaterial.RED_STAINED_GLASS_PANE.getMaterial(), 1);
        }
        DisplayItemConfiguration cancel = new DisplayItemConfiguration(cancelItem, 36, "/dshop item cancel");
        cancel.setDisplayName("&cCancel Item");

        DisplayItemConfiguration cancel2 = new DisplayItemConfiguration(cancelItem, 37, "/dshop item cancel");
        cancel2.setDisplayName("&cCancel Item");

        DisplayItemConfiguration cancel3 = new DisplayItemConfiguration(cancelItem, 38, "/dshop item cancel");
        cancel3.setDisplayName("&cCancel Item");

        editMenuItems = Arrays.asList(chance, amount, material, displayName, hideTags, command, listConditions, listPrices, listVariables, listLore, addCondition,
                addPrice, addVariable, addLore, save, save2, save3, cancel, cancel2, cancel3);
    }

    private void setupMainMenuFillItem() {
        ItemStack item;
        try {
            item = new ItemStack(UMaterial.BLACK_STAINED_GLASS_PANE.getMaterial(), 1, (short) 0, UMaterial.BLACK_STAINED_GLASS_PANE.getData());
        } catch (IllegalArgumentException e) {
            item = new ItemStack(UMaterial.BLACK_STAINED_GLASS_PANE.getMaterial(), 1);
        }
        fillItem = new DisplayItemConfiguration(item, 0, null);
        fillItem.setDisplayName("&b");
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public String getMysqlUrl() {
        return mysqlUrl;
    }

    public String getMysqlUsername() {
        return mysqlUsername;
    }

    public String getMysqlPassword() {
        return mysqlPassword;
    }

    public String getMainMenuTitle() {
        return mainMenuTitle;
    }

    public int getMenuRows() {
        return menuRows;
    }

    public List<DisplayItemConfiguration> getMainMenuItems() {
        return mainMenuItems;
    }

    public boolean isFillEmpty() {
        return fillEmpty;
    }

    public DisplayItemConfiguration getFillItem() {
        return fillItem;
    }

    public int getRefreshAfter() {
        return refreshAfter;
    }

    public List<DisplayItemConfiguration> getEditMenuItems() {
        return editMenuItems;
    }

    public String getEditMenuTitle() {
        return editMenuTitle;
    }

    public List<CurrencyYAMLConfiguration> getCurrencies() {
        return currencies;
    }

    public String getCategoryMenuTitle(String category) {
        return categoryMenuTitle.replace("{category_name}", category);
    }

    public int getEditItemFinalPosition() {
        return editItemFinalPosition;
    }

    public List<DisplayItemConfiguration> getCategoryMenuItems() {
        return categoryMenuItems;
    }

    public double getRefreshCategoryCost() {
        return refreshCategoryCost;
    }

    private CurrencyYAMLConfiguration refreshCurrencyConfiguration;

    public CurrencyYAMLConfiguration getRefreshCurrency() {
        if (refreshCurrencyConfiguration == null)
            for (CurrencyYAMLConfiguration currency : currencies) {
                if (currency.getName().equals(refreshCurrency)) {
                    refreshCurrencyConfiguration = currency;
                    break;
                }
            }
        return refreshCurrencyConfiguration;
    }
}
