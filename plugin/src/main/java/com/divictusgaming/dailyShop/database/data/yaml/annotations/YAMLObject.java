package com.divictusgaming.dailyShop.database.data.yaml.annotations;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface YAMLObject 
{
	Class<? extends DatabaseConfiguration> value();
}
