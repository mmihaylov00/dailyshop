package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLColumn;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLConstraints;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;
import com.divictusgaming.dailyShop.enumerate.ColumnType;
import com.divictusgaming.dailyShop.util.StringFormatter;
import com.divictusgaming.dailyShop.util.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class DisplayItemConfiguration extends DatabaseConfiguration {

    @MySQLColumn(ColumnType.VARCHAR)
    @MySQLConstraints("NOT NULL UNIQUE")
    @YAMLPath(path = "command")
    private String command;

    @MySQLColumn(ColumnType.VARCHAR)
    @MySQLConstraints("NOT NULL DEFAULT 'unknown'")
    @YAMLPath(path = "item-display-name")
    private String displayName;

    @MySQLColumn(ColumnType.VARCHAR)
    @MySQLConstraints("NOT NULL DEFAULT 'STONE'")
    @YAMLPath(path = "item-material")
    private Material material;

    @MySQLColumn(ColumnType.INT)
    @YAMLPath(path = "item-data")
    private Byte data;

    @MySQLColumn(ColumnType.INT)
    @MySQLConstraints("NOT NULL DEFAULT 1")
    @YAMLPath(path = "item-amount")
    private int amount;

    @MySQLColumn(ColumnType.INT)
    @YAMLPath(path = "item-position")
    private int position;

    @MySQLColumn(ColumnType.VARCHAR)
    @YAMLPath(path = "item-skull")
    private String skull;

    @MySQLColumn(ColumnType.TEXT)
    @YAMLPath(path = "item-lore")
    private List<String> lore;

    @MySQLColumn(ColumnType.BOOL)
    @MySQLConstraints("NOT NULL DEFAULT false")
    @YAMLPath(path = "item-is-enchanted")
    private boolean isEnchanted;

    @MySQLColumn(ColumnType.BOOL)
    @MySQLConstraints("NOT NULL DEFAULT false")
    @YAMLPath(path = "hide-tags")
    private boolean hideTags;

    private ItemStack item;

    public DisplayItemConfiguration() {
    }

    public DisplayItemConfiguration(ItemStack item, int position, String command) {
        this.setItem(item);
        if (lore == null) this.lore = new ArrayList<>();
        this.position = position;
        this.command = command;
    }

    public void setItem(ItemStack item) {
        this.item = null;
        hideTags = false;
        ItemMeta meta = item.getItemMeta();
        this.displayName = meta.getDisplayName();
        this.lore = meta.getLore();
        this.isEnchanted = meta.hasEnchants();
        this.amount = item.getAmount();
        this.setMaterial(item);
    }

    private static final ItemFlag[] flags = ItemFlag.values();

    public ItemStack getItem() {
        if (item != null) return item;
        ItemStack itemStack;

        try {
            itemStack = new ItemStack(material, amount, (short) 0, data);
        } catch (IllegalArgumentException e) {
            itemStack = new ItemStack(material, amount);
        }

        itemStack = Main.getPlugin(Main.class).getVersionMain().setNmsTags(itemStack, getCommand());

        if (material == UMaterial.PLAYER_HEAD_ITEM.getMaterial()) {
            //todo set the skull
        }

        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta == null) itemMeta = Bukkit.getItemFactory().getItemMeta(material);
        if (lore != null && !lore.isEmpty()) {
            StringFormatter.format(lore);
            itemMeta.setLore(lore);
        }
        itemMeta.setDisplayName(StringFormatter.format(displayName));

        if (hideTags) itemMeta.addItemFlags(flags);


        if (isEnchanted) {
            itemMeta.addEnchant(Enchantment.LUCK, 1, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }

        itemStack.setAmount(amount);
        itemStack.setItemMeta(itemMeta);

        return item = itemStack;
    }


    public String getDisplayName() {
        return displayName == null ? material.name() : displayName;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(ItemStack item) {
        this.material = item.getType();
        this.data = item.getData() == null || item.getData().getData() == 0 ? null : item.getData().getData();
        if (material == UMaterial.PLAYER_HEAD.getMaterial()) material = UMaterial.PLAYER_HEAD_ITEM.getMaterial();

        if (material == UMaterial.PLAYER_HEAD_ITEM.getMaterial()) {
            //todo get the skull owner
        }
        if (this.item != null) this.item.setType(material);
        setUpdated(true);
    }

    public int getAmount() {
        return amount;
    }

    public int getPosition() {
        return position;
    }

    public String getSkull() {
        return skull;
    }

    public List<String> getLore() {
        return lore;
    }

    public void addLoreLine(String line){
        lore.add(line);
        this.item = null;
        setUpdated(true);
    }

    public void removeLoreLine(int index){
        lore.remove(index);
        this.item = null;
        setUpdated(true);
    }

    public String getCommand() {
        return command;
    }

    public boolean isEnchanted() {
        return isEnchanted;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
        item = null;
        setUpdated(true);
    }

    public void setAmount(int amount) {
        this.amount = amount;
        if (this.item != null) this.item.setAmount(amount);
        setUpdated(true);
    }

    public void setPosition(int position) {
        this.position = position;
        setUpdated(true);
    }

    public void setSkull(String skull) {
        this.skull = skull;
    }

    public void setLore(List<String> lore) {
        this.lore = lore;
    }

    public void setEnchanted(boolean enchanted) {
        isEnchanted = enchanted;
    }

    public void setCommand(String command) {
        this.command = command;
        setUpdated(true);
    }

    public boolean isHideTags() {
        return hideTags;
    }

    public void setHideTags(boolean hideTags) {
        this.hideTags = hideTags;
        item = null;
        setUpdated(true);
    }
}
