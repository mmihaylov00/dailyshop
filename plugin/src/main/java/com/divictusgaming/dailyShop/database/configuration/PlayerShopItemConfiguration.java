package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.CurrencyYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.ItemCostConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.PlayerShopItemPriceConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.PlayerShopItemVariableConfiguration;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLColumn;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLTable;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLTableIndex;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLObject;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;
import com.divictusgaming.dailyShop.enumerate.ColumnType;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.ChatColor;

import java.util.*;

@MySQLTable("player_items")
@MySQLTableIndex(indexName = "item", uniqueFields = "category, playerId, slot")
public class PlayerShopItemConfiguration extends DatabaseConfiguration {
    @MySQLColumn(ColumnType.VARCHAR)
    @YAMLPath(path = "name")
    private String name;

    @MySQLColumn(ColumnType.VARCHAR)
    @YAMLPath(path = "category")
    private String category;

    @MySQLColumn(ColumnType.INT)
    private int playerId;

    @YAMLObject(PlayerShopItemPriceConfiguration.class)
    @YAMLPath(path = "price")
    private List<PlayerShopItemPriceConfiguration> price;

    @YAMLObject(PlayerShopItemVariableConfiguration.class)
    @YAMLPath(path = "variables")
    private List<PlayerShopItemVariableConfiguration> variables;

    @MySQLColumn(ColumnType.VARCHAR)
    private String priceStr;

    @MySQLColumn(ColumnType.VARCHAR)
    private String variablesStr;

    @MySQLColumn(ColumnType.VARCHAR)
    @YAMLPath(path = "command")
    private String command;

    @MySQLColumn(ColumnType.INT)
    @YAMLPath(path = "slot")
    private int slot;

    public PlayerShopItemConfiguration() {
        price = new ArrayList<>();
        variables = new ArrayList<>();
        this.setUpdated(true);
    }

    public PlayerShopItemConfiguration(int playerId, ShopItemConfiguration item, int slot, double percent) {
        this();
        this.playerId = playerId;
        this.name = item.getName();
        this.category = item.getCategory();
        this.slot = slot;

        for (VariableConfiguration v : item.getVariables())
            this.variables.add(new PlayerShopItemVariableConfiguration(v, percent));


        for (ItemCostConfiguration itemCostConfiguration :
                item.getPrices().get(new Random().nextInt(item.getPrices().size())).getPrices())
            this.price.add(new PlayerShopItemPriceConfiguration(itemCostConfiguration, percent));


        StringJoiner joiner = new StringJoiner(";");
        for (PlayerShopItemPriceConfiguration playerShopItemPriceConfiguration : this.price)
            joiner.add(playerShopItemPriceConfiguration.getCurrency() + ":" + playerShopItemPriceConfiguration.getPrice());
        this.priceStr = joiner.toString();

        StringJoiner result = new StringJoiner(";");
        for (PlayerShopItemVariableConfiguration p : this.variables)
            result.add(p.getVariable() + ":" + p.getValue());
        this.variablesStr = result.toString();

        this.command = StringFormatter.replacePlaceholders(null, item.getCommand(), getPlaceholders());
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public int getPlayerId() {
        return playerId;
    }

    public List<PlayerShopItemPriceConfiguration> getPrice() {
        if (price.isEmpty() && priceStr != null && !priceStr.isEmpty()) {
            for (String s : priceStr.split(";")) {
                String[] price = s.split(":");
                try {
                    this.price.add(new PlayerShopItemPriceConfiguration(price[0], Double.parseDouble(price[1])));
                } catch (NumberFormatException ignored) {
                }
            }
        }
        return price;
    }

    public List<PlayerShopItemVariableConfiguration> getVariables() {
        if (variables.isEmpty() && variablesStr != null && !variablesStr.isEmpty()) {
            for (String s : variablesStr.split(";")) {
                String[] variable = s.split(":");
                try {
                    this.variables.add(new PlayerShopItemVariableConfiguration(variable[0], Integer.parseInt(variable[1])));
                } catch (NumberFormatException ignored) {
                }
            }
        }
        return variables;
    }

    public HashMap<String, String> getPlaceholders() {
        List<PlayerShopItemVariableConfiguration> variables = getVariables();
        HashMap<String, String> placeholders = new HashMap<>();
        for (PlayerShopItemVariableConfiguration s : variables)
            placeholders.put(s.getVariable(), s.getValue() + "");

        placeholders.put("cost", getPriceBeautyString());
        return placeholders;
    }

    private String getPriceBeautyString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.price.size(); i++) {
            PlayerShopItemPriceConfiguration price = this.price.get(i);

            CurrencyYAMLConfiguration currency = null;
            for (CurrencyYAMLConfiguration c : Main.getPlugin(Main.class).getConfiguration().getCurrencies()) {
                if (c.getName().equals(price.getCurrency())) {
                    currency = c;
                    break;
                }
            }

            if (currency != null) {
                String currencyColors = ChatColor.getLastColors(currency.getDisplayName());
                sb.append(currencyColors);

                if ((price.getPrice() * 10) / 10 == price.getPrice()) sb.append((int) price.getPrice());
                else sb.append(price.getPrice());

                sb.append(" ").append(currency.getDisplayName());

                if (i != this.price.size() - 1) sb.append(" ");
            }
        }
        return sb.toString();
    }

    public String getPriceStr() {
        return priceStr;
    }

    public String getVariablesStr() {
        return variablesStr;
    }

    public String getCommand() {
        return command;
    }

    public int getSlot() {
        return slot;
    }
}
