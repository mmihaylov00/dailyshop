package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.*;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.*;
import com.divictusgaming.dailyShop.enumerate.ColumnType;

import java.util.ArrayList;
import java.util.List;

@YAMLFileLocation(location = "users")
@YAMLPathPrefix(path = "users")
@YAMLFileName(name = "{uuid}.yml")
@MySQLTable("players")
public class PlayerConfiguration extends DatabaseConfiguration {

    @MySQLId
    private int id;

    @MySQLColumn(ColumnType.VARCHAR)
    @MySQLConstraints("NOT NULL UNIQUE")
    @YAMLPath(path = "uuid")
    private String uuid;

    @MySQLJoin(object = PlayerShopItemConfiguration.class, joinTableField = "playerId")
    @YAMLObject(PlayerShopItemConfiguration.class)
    @YAMLPath(path = "items")
    private List<PlayerShopItemConfiguration> items;

    public PlayerConfiguration(String uuid) {
        this();
        this.setUpdated(true);
        this.uuid = uuid;
    }

    public PlayerConfiguration() {
        this.items = new ArrayList<>();
    }

    public void addItem(PlayerShopItemConfiguration item){
        items.add(item);
        this.setUpdated(true);
    }

    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public List<PlayerShopItemConfiguration> getItems() {
        return items;
    }

    public boolean removeItem(PlayerShopItemConfiguration item) {
        List<PlayerShopItemConfiguration> playerShopItemConfigurations = this.items;
        for (int i = 0; i < playerShopItemConfigurations.size(); i++)
            if (playerShopItemConfigurations.get(i).getSlot() == item.getSlot()) {
                this.items.remove(i);
                return true;
            }

        return false;
    }

}
