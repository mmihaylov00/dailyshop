package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

public class CurrencyYAMLConfiguration extends DatabaseConfiguration {

    @YAMLPath(path = "name")
    private String name;

    @YAMLPath(path = "display-name")
    private String displayName;

    @YAMLPath(path = "placeholder")
    private String placeholder;

    @YAMLPath(path = "take-command")
    private String takeCommand;

    @YAMLPath(path = "is-rounded")
    private Boolean isRounded;

    public CurrencyYAMLConfiguration() {
    }

    public CurrencyYAMLConfiguration(String name, String displayName, String placeholder, String takeCommand, Boolean isRounded) {
        this.name = name;
        this.displayName = displayName;
        this.placeholder = placeholder;
        this.takeCommand = takeCommand;
        this.isRounded = isRounded;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String getTakeCommand() {
        return takeCommand;
    }

    public Boolean getRounded() {
        return isRounded;
    }
}
