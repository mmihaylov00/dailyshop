package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.configuration.VariableConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

import java.util.Random;

public class PlayerShopItemVariableConfiguration extends DatabaseConfiguration {
    @YAMLPath(path = "variable")
    private String variable;

    @YAMLPath(path = "value")
    private int value;

    public PlayerShopItemVariableConfiguration() {
    }

    public PlayerShopItemVariableConfiguration(String currency, int value) {
        this.variable = currency;
        this.value = value;
    }

    public PlayerShopItemVariableConfiguration(VariableConfiguration v, double percent) {
        this.variable = v.getVariable();
        double value = v.getMax() - v.getMin();
        value = v.getMin() + (value * percent);

        Random random = new Random();
        if (random.nextBoolean()) {
            value -= v.getMax() * random.nextInt(15) * 0.01;
            if (value < v.getMin()) value = v.getMin();
            this.value = (int) Math.floor(value);
        } else {
            value += v.getMax() * random.nextInt(15) * 0.01;
            if (value > v.getMax()) value = v.getMax();
            this.value = (int) Math.ceil(value);
        }
    }

    public String getVariable() {
        return variable;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return variable + ":" + value;
    }
}
