package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

public class VariableConfiguration extends DatabaseConfiguration {
    @YAMLPath(path = "variable-name")
    private String variable;

    @YAMLPath(path = "min-value")
    private Integer min;

    @YAMLPath(path = "max-value")
    private Integer max;

    public VariableConfiguration() {
    }

    public VariableConfiguration(String variable, Integer min, Integer max) {
        this.variable = variable;
        this.min = min;
        this.max = max;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    @Override
    public String toString() {
        return variable + ": " + min + " - " + max;
    }
}
