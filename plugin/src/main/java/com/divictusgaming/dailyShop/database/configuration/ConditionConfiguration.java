package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

public class ConditionConfiguration extends DatabaseConfiguration {
    @YAMLPath(path = "placeholder")
    private String placeholder;

    @YAMLPath(path = "min")
    private double min;

    @YAMLPath(path = "max")
    private double max;

    public ConditionConfiguration() {
    }

    public ConditionConfiguration(String placeholder, double min, double max) {
        this.placeholder = placeholder;
        this.min = min;
        this.max = max;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    @Override
    public String toString() {
        return min + " ≤ " + placeholder + " (%" + placeholder + "%) ≤ " + max;
    }
}
