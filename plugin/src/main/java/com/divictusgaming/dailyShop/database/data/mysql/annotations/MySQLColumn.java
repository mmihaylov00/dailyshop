package com.divictusgaming.dailyShop.database.data.mysql.annotations;

import com.divictusgaming.dailyShop.enumerate.ColumnType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MySQLColumn {
    ColumnType value();
}
