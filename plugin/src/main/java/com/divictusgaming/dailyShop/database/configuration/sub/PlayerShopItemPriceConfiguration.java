package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

import java.util.Random;

public class PlayerShopItemPriceConfiguration extends DatabaseConfiguration {
    @YAMLPath(path = "currency")
    private String currency;

    @YAMLPath(path = "price")
    private double price;

    public PlayerShopItemPriceConfiguration() {
    }

    public PlayerShopItemPriceConfiguration(String currency, double price) {
        this.currency = currency;
        this.price = price;
    }

    public PlayerShopItemPriceConfiguration(ItemCostConfiguration price, double percent) {
        Random random = new Random();
        this.currency = price.getCurrencyName();

        double value = price.getMax() - price.getMin();
        value = price.getMin() + (value * percent);

        if (random.nextBoolean()) {
            value -= price.getMax() * random.nextInt(15) * 0.01;
            if (value < price.getMin()) value = price.getMin();
        } else {
            value += price.getMax() * random.nextInt(15) * 0.01;
            if (value > price.getMax()) value = price.getMax();
        }

        CurrencyYAMLConfiguration c = null;
        for (CurrencyYAMLConfiguration curr : Main.getPlugin(Main.class).getConfiguration().getCurrencies()) {
            if (curr.getName().equals(currency)) {
                c = curr;
                break;
            }
        }

        if (c != null && c.getRounded() == Boolean.TRUE)
            this.price = Math.ceil(value);
        else
            this.price = Math.ceil(value * 100) / 100;
    }

    public String getCurrency() {
        return currency;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return currency + ":" + price;
    }
}
