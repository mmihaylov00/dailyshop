package com.divictusgaming.dailyShop.database;

import com.divictusgaming.dailyShop.database.data.mysql.DataSetFieldMapper;

import java.sql.ResultSet;

public abstract class DatabaseConfiguration {
    private boolean isUpdated = false;

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean isUpdated) {
        this.isUpdated = isUpdated;
    }

    @SuppressWarnings("unchecked")
    public <T extends DatabaseConfiguration> T mapFieldsFromMySQL(ResultSet set, IDatabase.LoadDataCallback<T> callback){
        DataSetFieldMapper.mapFields(set, this);
        callback.onQueryDone((T) this, null);
        return (T) this;
    }

    public void setId(int id){
        //override only for sql tables
    }
}
