package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLObject;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ItemCostWrapperConfiguration extends DatabaseConfiguration {

    @YAMLObject(ItemCostConfiguration.class)
    @YAMLPath(path = "prices")
    private List<ItemCostConfiguration> prices;


    public ItemCostWrapperConfiguration() {
        this.prices = new ArrayList<>();
    }

    public ItemCostWrapperConfiguration(List<ItemCostConfiguration> prices) {
        this.prices = prices;
    }

    public List<ItemCostConfiguration> getPrices() {
        return prices;
    }

    public void setPrices(List<ItemCostConfiguration> prices) {
        this.prices = prices;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(" & ");
        for (ItemCostConfiguration price : prices)
            joiner.add(price.toString());
        return joiner.toString();
    }
}
