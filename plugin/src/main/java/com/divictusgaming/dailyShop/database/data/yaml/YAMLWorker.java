package com.divictusgaming.dailyShop.database.data.yaml;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.*;
import com.divictusgaming.dailyShop.exception.DataNotFoundException;
import com.divictusgaming.dailyShop.util.ReflectionHelper;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

public class YAMLWorker {
    private static final File PATH = Main.getPlugin(Main.class).getPluginsFolder();

    public static <T extends DatabaseConfiguration> void readFile(T configuration) throws DataNotFoundException {
        readFile(configuration, null);
    }

    public static <T extends DatabaseConfiguration> void readFile(T configuration, Map<String, String> placeholders) throws DataNotFoundException {
        File configFile = loadFile(configuration, placeholders);
        //check if the file is saved or not
        if (!configFile.exists())
            throw new DataNotFoundException();
        //load that file
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile);//attempt to load the file
        //get prefix
        String prefix = loadPrefix(configuration, placeholders);
        //start reading each field in the class & setting them
        readField(fileConfiguration, configuration, prefix);
    }

    public static <T extends DatabaseConfiguration> String getFileLocation(T configuration) {
        String fileLocation = "";
        if (configuration.getClass().isAnnotationPresent(YAMLFileLocation.class)) {
            fileLocation = configuration.getClass().getAnnotation(YAMLFileLocation.class).location(); //fetch location string
            //check if it begins with /
            if (fileLocation.charAt(0) != '/')
                fileLocation = "/" + fileLocation;

        }
        return fileLocation;
    }

    private static final Pattern FILE_NAME_PATTERN = Pattern.compile("^.+\\.yml$");

    public static <T extends DatabaseConfiguration> String getFileName(T configuration) {
        if (!configuration.getClass().isAnnotationPresent(YAMLFileName.class)) {
            throw new RuntimeException("Missing @YAMLFileName annotation on the " + configuration.getClass().getSimpleName() + " class!");
        }
        String fileName = configuration.getClass().getAnnotation(YAMLFileName.class).name();
        if (!fileName.matches(FILE_NAME_PATTERN.pattern())) {
            if (fileName.endsWith(".")) fileName += "yml";
            else fileName += ".yml";
        }
        return fileName;
    }

    public static <T extends DatabaseConfiguration> void saveFile(T configuration) {
        saveFile(configuration, null);
    }

    //adds the missing fields to the configuration
    public static <T extends DatabaseConfiguration> void addMissing(T configuration) {
        File configFile = loadFile(configuration); //load file from the class itself
        String prefix = loadPrefix(configuration, null); //load path prefix from class
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file

        saveFields(fileConfiguration, configuration, prefix, false); //save all fields declared in class to file
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T extends DatabaseConfiguration> void saveFile(T configuration, Map<String, String> placeholders) {
        File configFile = loadFile(configuration, placeholders); //load file from the class itself
        String prefix = loadPrefix(configuration, placeholders); //load path prefix from class
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file

        saveFields(fileConfiguration, configuration, prefix, true); //save all fields declared in class to file
        //save the file itself on machine
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("rawtypes")
    public static <T extends DatabaseConfiguration> void saveSingleField(T configuration, String fieldName) //modified some things (added YAMLFileLocation annotaion so sorting files is easier)
    {
        File configFile = loadFile(configuration, null);
        String prefix = loadPrefix(configuration, null);
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file
        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass())) {
            if (declaredField.getName().equalsIgnoreCase(fieldName)) {
                declaredField.setAccessible(true); //change it to accessable
                if (declaredField.isAnnotationPresent(YAMLPath.class)) {
                    String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path
                    try {
                        if (declaredField.get(configuration) == null) {
                            break;
                        }
                        if (declaredField.getType() == Location.class) {
                            Object o = declaredField.get(configuration);
                            fileConfiguration.set(prefix + path, StringFormatter.locationToString((Location) o, true));
                        } else if (declaredField.getType() == String.class ||
                                declaredField.getType() == double.class ||
                                declaredField.getType() == Double.class ||
                                declaredField.getType() == int.class ||
                                declaredField.getType() == Integer.class ||
                                declaredField.getType() == byte.class ||
                                declaredField.getType() == Byte.class ||
                                declaredField.getType() == boolean.class ||
                                declaredField.getType() == Boolean.class) {
                            Object o = declaredField.get(configuration);
                            fileConfiguration.set(prefix + path, o);
                        } else if (declaredField.getType() == Material.class) {
                            Material o = (Material) declaredField.get(configuration);
                            fileConfiguration.set(prefix + path, o.name());
                        } else if (declaredField.getType().isEnum()) {
                            Enum o = (Enum) declaredField.get(configuration);
                            if (o != null) fileConfiguration.set(prefix + path, o.name());    //save only its name
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                declaredField.setAccessible(false); //reset field to inaccessable
                break;
            }
        }
        //save the file itself on machine
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static DatabaseConfiguration readField(FileConfiguration fileConfiguration, DatabaseConfiguration configuration, String prefix) {
        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass())) //fetch all declared fields
        {
            declaredField.setAccessible(true); //change them to accessable
            if (declaredField.isAnnotationPresent(YAMLPath.class)) {
                String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path
                //check if path is set
                if (!fileConfiguration.isSet(prefix + path))
                    continue;
                try {
                    if (declaredField.getType() == Location.class) {
                        Object o = fileConfiguration.get(prefix + path);
                        declaredField.set(configuration, StringFormatter.stringToLocation((String) o));
                    } else if (declaredField.getType() == Material.class) {
                        String o = fileConfiguration.getString(prefix + path).toUpperCase(Locale.ROOT);
                        Material material = Material.getMaterial(o);
                        declaredField.set(configuration, material);
                    } else if (fileConfiguration.get(prefix + path).getClass().equals(declaredField.getType())) {
                        Object o = fileConfiguration.get(prefix + path);//get the object
                        declaredField.set(configuration, o); //set this in configuration
                    } else if (declaredField.getType().isEnum()) {
                        //get enum name from file
                        String enumName = fileConfiguration.getString(prefix + path);
                        //get enum object
                        Class<? extends Enum> o = (Class<? extends Enum>) declaredField.getType();
                        //set enum in declared fields
                        declaredField.set(configuration, Enum.valueOf(o, enumName));
                    } else if (List.class.isAssignableFrom(declaredField.getType())) {
                        if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                            Class<?> c = declaredField.getAnnotation(YAMLObject.class).value();
                            //get length of list
                            List<DatabaseConfiguration> list = new ArrayList<>();
                            try {
                                int length = fileConfiguration.getConfigurationSection(prefix + path).getKeys(false).size();
                                //create a new list
                                for (int i = 0; i < length; i++) {
                                    //create an instance of the class
                                    try {
                                        list.add(readField(fileConfiguration, (DatabaseConfiguration) c.newInstance(), prefix + path + "." + i + "."));
                                    } catch (InstantiationException e) {
                                        e.printStackTrace();
                                    }
                                }
                                //set this list to field
                            } catch (NullPointerException e) {
                                try {
                                    list.add(readField(fileConfiguration, (DatabaseConfiguration) c.newInstance(), prefix + path));
                                } catch (InstantiationException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            declaredField.set(configuration, list);
                        } else {
                            declaredField.set(configuration, fileConfiguration.get(prefix + path));
                        }
                    } else if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                        //get class name (cant fetch from list cause runtimeException)
                        try {
                            Class<?> c = declaredField.getAnnotation(YAMLObject.class).value();
                            declaredField.set(configuration, readField(fileConfiguration, (DatabaseConfiguration) c.newInstance(), prefix + path + "."));
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Object o = fileConfiguration.get(prefix + path);

                        if (declaredField.getType() == Integer.class) {
                            if (o instanceof Integer)
                                declaredField.set(configuration, o);
                            else if (o instanceof Double)
                                declaredField.set(configuration, ((Double) o).intValue());

                        } else if (declaredField.getType() == Double.class) {
                            if (o instanceof Integer)
                                declaredField.set(configuration, ((Integer) o).doubleValue());
                            else if (o instanceof Double)
                                declaredField.set(configuration, o);
                        } else if (declaredField.getType() == Boolean.class) {
                            declaredField.set(configuration, o);
                        } else if (declaredField.getType() == Byte.class) {
                            declaredField.set(configuration, ((Integer) o).byteValue());
                        } else
                            declaredField.set(configuration, o);

                    }
                } catch (IllegalAccessException | ClassCastException e) {
                    e.printStackTrace();
                }
            }
            declaredField.setAccessible(false);
        }
        return configuration;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static void saveFields(FileConfiguration fileConfiguration, DatabaseConfiguration configuration, String prefix, boolean replaceExisting) {
        //fetch all declared fields
        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass())) {
            declaredField.setAccessible(true);
            //check if field had path annotation
            if (declaredField.isAnnotationPresent(YAMLPath.class)) {
                String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path

                try {
                    if (declaredField.get(configuration) == null) continue;

                    if (declaredField.getType() == String.class ||    //test if field is a store able type(can be set as is)
                            declaredField.getType() == Double.class ||
                            declaredField.getType() == double.class ||
                            declaredField.getType() == Byte.class ||
                            declaredField.getType() == byte.class ||
                            declaredField.getType() == Integer.class ||
                            declaredField.getType() == int.class ||
                            declaredField.getType() == Boolean.class ||
                            declaredField.getType() == boolean.class) {
                        Object o = declaredField.get(configuration);
                        //do we want to set it to the value from the object if it's not set
                        //used for adding missing values from the object to the file without resetting all other values
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            fileConfiguration.set(prefix + path, o);
                    } else if (declaredField.getType() == Material.class) {
                        Material material = (Material) declaredField.get(configuration);
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            fileConfiguration.set(prefix + path, material.name().toUpperCase(Locale.ROOT));
                    } else if (declaredField.getType() == Location.class) {
                        Object o = declaredField.get(configuration);
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            //we want to save it as a string
                            fileConfiguration.set(prefix + path, StringFormatter.locationToString((Location) o, true));

                    } else if (declaredField.getType().isEnum()) {
                        Enum o = (Enum) declaredField.get(configuration);
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            if (o != null) fileConfiguration.set(prefix + path, o.name());    //save only its name

                    } else if (List.class.isAssignableFrom(declaredField.getType())) {
                        if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                            List<DatabaseConfiguration> list = (List<DatabaseConfiguration>) declaredField.get(configuration);//get the list
                            for (int i = 0; i < list.size(); i++) {
                                DatabaseConfiguration yamlConfiguration = list.get(i);
                                saveFields(fileConfiguration, yamlConfiguration, prefix + path + "." + i + ".", replaceExisting); //save the field using recursion
                            }
                        } else {
                            Object o = declaredField.get(configuration);
                            if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                                if (o != null) fileConfiguration.set(prefix + path, o);
                        }
                    } else if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                        saveFields(fileConfiguration, (DatabaseConfiguration) declaredField.get(configuration), prefix + path + ".", replaceExisting);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            declaredField.setAccessible(false);
        }
    }

    private static File loadFile(DatabaseConfiguration configuration) {
        return loadFile(configuration, null);
    }

    public static File loadFile(DatabaseConfiguration configuration, Map<String, String> placeholders) {
        String fileLocation = "";
        if (configuration.getClass().isAnnotationPresent(YAMLFileLocation.class)) {
            fileLocation = configuration.getClass().getAnnotation(YAMLFileLocation.class).location(); //fetch location string
            //check if it begins with /
            if (fileLocation.charAt(0) != '/')
                fileLocation = "/" + fileLocation;

        }
        if (!configuration.getClass().isAnnotationPresent(YAMLFileName.class)) {
            throw new RuntimeException("Missing @YAMLFileName annotation on " + configuration.getClass().getSimpleName() + " class!");
        }
        String fileName = configuration.getClass().getAnnotation(YAMLFileName.class).name();
        //if placeholders == null, skip
        if (placeholders != null) {
            for (Map.Entry<String, String> entry : placeholders.entrySet()) {
                fileName = fileName.replaceAll("\\{" + entry.getKey() + "}", entry.getValue());
            }
        }
        if (!fileName.matches(FILE_NAME_PATTERN.pattern())) {
            if (fileName.endsWith(".")) fileName += "yml";
            else fileName += ".yml";
        }

        return new File(PATH + fileLocation, fileName);

    }

    private static String loadPrefix(DatabaseConfiguration configuration, Map<String, String> placeholders) {
        if (!configuration.getClass().isAnnotationPresent(YAMLPathPrefix.class)) {
            return "";
        }
        String prefix = configuration.getClass().getAnnotation(YAMLPathPrefix.class).path();
        if (!prefix.endsWith("."))
            prefix += ".";
        if (placeholders != null) {
            for (Map.Entry<String, String> entry : placeholders.entrySet()) {
                prefix = prefix.replaceAll("\\{" + entry.getKey() + "}", entry.getValue());
            }
        }
        return prefix;
    }

    public static void deleteFile(DatabaseConfiguration configuration, Map<String, String> placeholders) {
        File file = loadFile(configuration, placeholders);
        file.delete();
    }
}
