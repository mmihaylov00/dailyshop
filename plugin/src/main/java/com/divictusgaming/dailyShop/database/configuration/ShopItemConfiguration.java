package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.configuration.sub.DisplayItemConfiguration;
import com.divictusgaming.dailyShop.database.configuration.sub.ItemCostWrapperConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLObject;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ShopItemConfiguration extends DisplayItemConfiguration {
    @YAMLPath(path = "name")
    private String name;

    @YAMLPath(path = "percentage")
    private Double percentage;

    @YAMLPath(path = "category")
    private String category;

    @YAMLObject(ConditionConfiguration.class)
    @YAMLPath(path = "conditions")
    private List<ConditionConfiguration> conditions;

    @YAMLObject(ItemCostWrapperConfiguration.class)
    @YAMLPath(path = "prices")
    private List<ItemCostWrapperConfiguration> prices;

    @YAMLObject(VariableConfiguration.class)
    @YAMLPath(path = "variables")
    private List<VariableConfiguration> variables;


    public ShopItemConfiguration() {
        super();
        this.conditions = new ArrayList<>();
        this.prices = new ArrayList<>();
        this.variables = new ArrayList<>();
    }

    public ShopItemConfiguration(ItemStack item, String name, String category) {
        super(item, 0, null);
        this.name = name;
        this.category = category;
        this.conditions = new ArrayList<>();
        this.prices = new ArrayList<>();
        this.variables = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public Double getPercentage() {
        return percentage;
    }

    public List<ConditionConfiguration> getConditions() {
        return conditions;
    }

    public String getConditionsString() {
        StringBuilder sb = new StringBuilder();
        for (ConditionConfiguration condition : conditions)
            sb.append(condition.toString()).append("\\n");

        return sb.toString();
    }

    public List<ItemCostWrapperConfiguration> getPrices() {
        return prices;
    }

    public String getPricesString() {
        StringBuilder sb = new StringBuilder();
        for (ItemCostWrapperConfiguration price : prices)
            sb.append(price.toString()).append("\\n");

        return sb.toString();
    }

    public List<VariableConfiguration> getVariables() {
        return variables;
    }

    public String getVariablesString() {
        StringBuilder sb = new StringBuilder();
        for (VariableConfiguration variable : variables)
            sb.append(variable.toString()).append("\\n");

        return sb.toString();
    }

    public String getLoreString() {
        StringBuilder sb = new StringBuilder();
        for (String l : getLore())
            sb.append(l).append("\\n");

        return sb.toString();
    }

    public String getCategory() {
        return category;
    }
}
