package com.divictusgaming.dailyShop.database.implementation;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.IDatabase;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.YAMLWorker;
import com.divictusgaming.dailyShop.exception.DataNotFoundException;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileDatabase implements IDatabase {

    @Override
    public <T extends DatabaseConfiguration> void loadData(String searchKey, String searchValue,
                                                           Class<T> outputClass, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Map<String, String> placeholder = new HashMap<>();
                placeholder.put(searchKey, searchValue);
                T output;
                try {
                    output = outputClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    YAMLWorker.readFile(output, placeholder);
                } catch (DataNotFoundException e) {
                    callback.onError(e);
                    return;
                }
                StringFormatter.formatAllFields(output);
                callback.onQueryDone(output, null);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void saveData(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                saveDataSync(searchKey, searchValue, configuration);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void saveDataSync(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        if (configuration.isUpdated()) {
            Map<String, String> placeholder = new HashMap<>();
            placeholder.put(searchKey, searchValue);
            YAMLWorker.saveFile(configuration, placeholder);
            configuration.setUpdated(false);
        }
    }

    @Override
    public <T extends DatabaseConfiguration> void saveData(String searchKey, String searchValue, T configuration, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (configuration.isUpdated()) {
                    Map<String, String> placeholder = new HashMap<>();
                    placeholder.put(searchKey, searchValue);
                    YAMLWorker.saveFile(configuration, placeholder);
                    configuration.setUpdated(false);
                    callback.onQueryDone(configuration, null);
                }
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void deleteData(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                HashMap<String, String> placeholder = new HashMap<>();
                placeholder.put(searchKey, searchValue);
                YAMLWorker.deleteFile(new CategoryConfiguration(), placeholder);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void clearAllData(Class<? extends DatabaseConfiguration> c) {
        new BukkitRunnable() {
            @Override
            public void run() {
                List<String> names = getAllFileNames(c);

                for (String name : names) {
                    HashMap<String, String> placeholder = new HashMap<>();
                    placeholder.put("name", name);
                    try {
                        YAMLWorker.deleteFile(c.newInstance(), placeholder);
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    private List<String> getAllFileNames(Class<? extends DatabaseConfiguration> c) {

        String fileLocation = null;

        try {
            fileLocation = YAMLWorker.getFileLocation(c.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        File folder = new File(Main.getPlugin(Main.class).getPluginsFolder() + fileLocation + "/");

        String[] fileNames = folder.list();
        ArrayList<String> names = new ArrayList<>();

        if (fileNames != null) {
            for (String s : fileNames) {
                if (s.endsWith(".yml"))
                    names.add(s.replace(".yml", ""));
                else if (s.endsWith("."))
                    names.add(s.replace(".", ""));
                else
                    names.add(s);
            }
        }
        return names;
    }

    @Override
    public <T extends DatabaseConfiguration> void loadAll(Class<T> c, LoadDataCallback<T> callback) {
        loadAll(null, null, c, callback);
    }

    @Override
    public <T extends DatabaseConfiguration> void loadAll(String searchKey, Object searchValue, Class<T> c, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                ArrayList<T> output = new ArrayList<>();
                List<String> names = getAllFileNames(c);

                for (String name : names) {
                    HashMap<String, String> placeholder = new HashMap<>();
                    placeholder.put("name", name);
                    try {
                        T databaseConfiguration = c.newInstance();
                        YAMLWorker.readFile(databaseConfiguration, placeholder);
                        if (searchKey == null || databaseConfiguration.getClass().getField(searchKey)
                                .get(databaseConfiguration).equals(searchValue)) {
                            output.add(databaseConfiguration);
                            StringFormatter.formatAllFields(databaseConfiguration);
                        }
                    } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
                        e.printStackTrace();
                    } catch (DataNotFoundException ignored) { //can't happen
                    }
                }

                callback.onQueryDone(null, output);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public boolean isConnected() {
        return true;
    }
}
