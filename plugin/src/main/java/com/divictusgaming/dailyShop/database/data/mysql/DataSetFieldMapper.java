package com.divictusgaming.dailyShop.database.data.mysql;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLColumn;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLId;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.MySQLJoin;
import com.divictusgaming.dailyShop.util.ReflectionHelper;
import org.bukkit.Material;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataSetFieldMapper {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void mapFields(ResultSet set, DatabaseConfiguration configuration) {
        int id = 0;
        for (Field field : ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass())) {
            try {
                field.setAccessible(true);
                if (field.isAnnotationPresent(MySQLId.class)) {
                    id = set.getInt("id");
                    field.setInt(configuration, id);
                } else if (field.isAnnotationPresent(MySQLColumn.class)) {
                    switch (field.getAnnotation(MySQLColumn.class).value()) {
                        case INT: {
                            field.setInt(configuration, set.getInt(field.getName()));
                            break;
                        }
                        case BOOL: {
                            field.setBoolean(configuration, set.getBoolean(field.getName()));
                            break;
                        }
                        case TEXT: {
                            String text = set.getString(field.getName());
                            if (text == null || text.isEmpty()) field.set(configuration, new ArrayList<>());
                            else {
                                List<String> list = new ArrayList<>();
                                Collections.addAll(list, text.split("\n"));
                                field.set(configuration, list);
                            }
                            break;
                        }
                        default:
                        case VARCHAR: {
                            if (field.getType() == Material.class)
                                field.set(configuration, Material.getMaterial(set.getString(field.getName())));
                            else
                                field.set(configuration, set.getString(field.getName()));
                            break;
                        }
                    }
                } else if (field.isAnnotationPresent(MySQLJoin.class)) {
                    MySQLJoin annotation = field.getAnnotation(MySQLJoin.class);
                    Main.getPlugin(Main.class).getDatabase()
                            .loadAll(annotation.joinTableField(), id, annotation.object(), (result, results) -> {
                                try {
                                    field.setAccessible(true);
                                    field.set(configuration, results);
                                    field.setAccessible(false);
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            });
                }
                field.setAccessible(false);
            } catch (IllegalAccessException | SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
