package com.divictusgaming.dailyShop.database.configuration;

import com.divictusgaming.dailyShop.database.configuration.sub.DisplayItemConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.*;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@YAMLFileLocation(location = "categories")
@YAMLPathPrefix(path = "category")
@YAMLFileName(name = "{name}.yml")
public class CategoryConfiguration extends DisplayItemConfiguration {

    @YAMLPath(path = "name")
    private String name;

    @YAMLPath(path = "permission")
    private String permission;

    @YAMLPath(path = "item-slots")
    private List<Integer> itemSlots;

    @YAMLObject(ShopItemConfiguration.class)
    @YAMLPath(path = "items")
    private List<ShopItemConfiguration> shopItems;

    public CategoryConfiguration() {
        super();
        this.shopItems = new ArrayList<>();
    }

    public CategoryConfiguration(ItemStack item, String name, int position, String command) {
        super(item, position, command);
        this.name = name;
        this.shopItems = new ArrayList<>();
        this.itemSlots = new ArrayList<>();
        setHideTags(true);
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
        setUpdated(true);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShopItemConfiguration> getShopItems() {
        return shopItems;
    }
    @Override
    public String getCommand(){
        return "/" + super.getCommand();
    }

    public String getPlainCommand(){
        return super.getCommand();
    }

    public String getShopItemNames(){
        StringJoiner joiner = new StringJoiner(", ");
        for (ShopItemConfiguration configuration : this.getShopItems()) {
            String configurationName = configuration.getName();
            joiner.add(configurationName);
        }
        return joiner.toString();
    }

    public List<Integer> getItemSlots() {
        return itemSlots;
    }

    public void setItemSlots(List<Integer> itemSlots) {
        this.itemSlots = itemSlots;
        setUpdated(true);
    }
}
