package com.divictusgaming.dailyShop.database.configuration.sub;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.data.yaml.annotations.YAMLPath;

public class ItemCostConfiguration extends DatabaseConfiguration {

    @YAMLPath(path = "currency-name")
    private String currencyName;
    @YAMLPath(path = "min-value")
    private double min;
    @YAMLPath(path = "max-value")
    private double max;

    public ItemCostConfiguration() {
    }

    public ItemCostConfiguration(String currencyName, double min, double max) {
        this.currencyName = currencyName;
        this.min = min;
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    @Override
    public String toString() {
        return min + " - " + max + " " + currencyName;
    }
}
