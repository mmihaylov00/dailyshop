package com.divictusgaming.dailyShop.database.implementation;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import com.divictusgaming.dailyShop.database.IDatabase;
import com.divictusgaming.dailyShop.database.configuration.ConfigYAMLConfiguration;
import com.divictusgaming.dailyShop.database.configuration.PlayerConfiguration;
import com.divictusgaming.dailyShop.database.configuration.PlayerShopItemConfiguration;
import com.divictusgaming.dailyShop.database.data.mysql.annotations.*;
import com.divictusgaming.dailyShop.exception.DataNotFoundException;
import com.divictusgaming.dailyShop.util.ReflectionHelper;
import com.divictusgaming.dailyShop.util.StringFormatter;
import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

public class SQLDatabase implements IDatabase {
    private Connection connection;
    private boolean isConnected = true;
    private HashMap<Class<? extends DatabaseConfiguration>, String> saveQueries;

    private static final Class<? extends DatabaseConfiguration>[] tables = new Class[]{
            PlayerConfiguration.class,
            PlayerShopItemConfiguration.class,
    };

    public SQLDatabase() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        saveQueries = new HashMap<>();
        ConfigYAMLConfiguration config = Main.getPlugin(Main.class).getConfiguration();
        connect(config.getMysqlUrl(), config.getMysqlUsername(), config.getMysqlPassword());
    }

    private void connect(String url, String username, String password) {
        try {
            connection = DriverManager.getConnection(url, username, password);

            for (Class<? extends DatabaseConfiguration> table : tables)
                generateTable(table);

        } catch (SQLException e) {
            if (!url.endsWith("&characterEncoding=latin1")) {
                connect(url + "&characterEncoding=latin1", username, password);
            } else {
                isConnected = false;
                e.printStackTrace();
            }
        }
    }


    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private <T extends DatabaseConfiguration> void generateTable(Class<T> c) {
        String sql = "CREATE TABLE IF NOT EXISTS " + getTableName(c) + "(id int PRIMARY KEY NOT NULL AUTO_INCREMENT);";
        try {
            connection.prepareStatement(sql).execute();
            Log.info(sql);
            generateColumns(c);
            addTableConstraints(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private <T extends DatabaseConfiguration> void addTableConstraints(Class<T> c) {
        if (c.isAnnotationPresent(MySQLTableIndex.class)) {
            MySQLTableIndex annotation = c.getAnnotation(MySQLTableIndex.class);
            String sql = "CREATE UNIQUE INDEX " + annotation.indexName() + " ON " + getTableName(c) +
                    " (" + annotation.uniqueFields() + ");";
            try {
                Log.info(sql);
                connection.prepareStatement(sql).execute();
            } catch (SQLException e) {
                //thrown if constraint exists
            }
        }
    }

    private <T extends DatabaseConfiguration> void generateColumns(Class<T> c) {
        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), c)) {
            if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                declaredField.setAccessible(true);
                String constraints = declaredField.isAnnotationPresent(MySQLConstraints.class) ?
                        declaredField.getAnnotation(MySQLConstraints.class).value() : "";
                String sql = "ALTER TABLE " + getTableName(c) + " ADD " + declaredField.getName() + " " +
                        declaredField.getAnnotation(MySQLColumn.class).value().getMysqlType() + " " + constraints + ";";
                try {
                    Log.info(sql);
                    connection.prepareStatement(sql).execute();
                } catch (SQLException e) {
                    //thrown if column already exists
                }
                declaredField.setAccessible(false);
            }
        }
    }

    @Override
    public <T extends DatabaseConfiguration> void loadData(String searchKey, String searchValue, Class<T> outputClass, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    String sql = "SELECT * FROM " + getTableName(outputClass) + " WHERE " + searchKey + " = ?;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setString(1, searchValue);
                    Log.info(preparedStatement.toString());
                    ResultSet result = preparedStatement.executeQuery();

                    if (result.next()) {
                        T databaseConfiguration = outputClass.newInstance().mapFieldsFromMySQL(result, (r, results) -> {
                            StringFormatter.formatAllFields(r);
                            callback.onQueryDone(r, null);
                        });
                        return;
                    }

                } catch (SQLException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                callback.onError(new DataNotFoundException());
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    private String prepareSaveQuery(DatabaseConfiguration configuration) {
        String s = saveQueries.get(configuration.getClass());
        if (s != null) return s;

        StringBuilder sb = new StringBuilder("INSERT INTO ").append(getTableName(configuration.getClass()));

        List<Field> declaredFields = ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass());
        StringJoiner columns = new StringJoiner(", ");
        StringJoiner values = new StringJoiner(", ");
        StringJoiner update = new StringJoiner(", ");

        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                declaredField.setAccessible(true);
                String fieldName = declaredField.getName();
                columns.add(fieldName);
                values.add("?");
                update.add(fieldName + "=VALUES(" + fieldName + ")");
                declaredField.setAccessible(false);
            }
        }
        String query = sb.append(" (").append(columns.toString())
                .append(") VALUES(").append(values.toString())
                .append(") ON DUPLICATE KEY UPDATE ").append(update.toString()).toString();

        saveQueries.put(configuration.getClass(), query);
        Log.info(query);
        return query;
    }

    private String prepareSaveQuery(ArrayList<DatabaseConfiguration> configuration, Class<? extends DatabaseConfiguration> c) {
        String s = saveQueries.get(configuration.getClass());
        if (s != null) return s;

        StringBuilder sb = new StringBuilder("INSERT INTO ").append(getTableName(c));
        StringBuilder columns = new StringBuilder(" (");
        StringBuilder values = new StringBuilder(") VALUES(");
        StringBuilder update = new StringBuilder(") ON DUPLICATE KEY UPDATE ");

        List<Field> declaredFields = ReflectionHelper.getAllFields(new ArrayList<>(), c);
        boolean first = true;
        for (DatabaseConfiguration databaseConfiguration : configuration) {
            for (Field declaredField : declaredFields) {
                if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                    declaredField.setAccessible(true);
                    String fieldName = declaredField.getName();
                    if (first) columns.append(fieldName).append(", ");
                    values.append("?").append(", ");
                    if (first) update.append(fieldName).append("=VALUES(").append(fieldName).append(")").append(", ");
                    declaredField.setAccessible(false);
                }
            }
            first = false;
            values.delete(values.length() - 2, values.length());
            values.append("), (");
        }

        columns.delete(columns.length() - 2, columns.length());
        values.delete(values.length() - 4, values.length());
        update.delete(update.length() - 2, update.length());
        String query = sb.append(columns).append(values).append(update).toString();

        saveQueries.put(c, query);
        Log.info(query);
        return query;
    }

    @SuppressWarnings("unchecked")
    private int fillAllFields(PreparedStatement preparedStatement, DatabaseConfiguration configuration, int counter) {
        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass())) {
            try {
                if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                    declaredField.setAccessible(true);
                    switch (declaredField.getAnnotation(MySQLColumn.class).value()) {
                        case INT:
                            Integer i = null;
                            Byte bt = null;
                            if (declaredField.getType() == Integer.class)
                                i = (Integer) declaredField.get(configuration);
                            else if (declaredField.getType() == Byte.class)
                                bt = (Byte) declaredField.get(configuration);
                            else if (declaredField.getType() == byte.class)
                                bt = declaredField.getByte(configuration);
                            else
                                i = declaredField.getInt(configuration);

                            if (i != null) preparedStatement.setInt(counter++, i);
                            if (bt != null) preparedStatement.setByte(counter++, bt);
                            break;
                        case BOOL:
                            Boolean b;
                            if (declaredField.getType() == Boolean.class)
                                b = (Boolean) declaredField.get(configuration);
                            else
                                b = declaredField.getBoolean(configuration);

                            preparedStatement.setBoolean(counter++, b);
                            break;
                        case TEXT:
                            ArrayList<String> arr = (ArrayList<String>) declaredField.get(configuration);
                            if (arr == null) arr = new ArrayList<>();
                            preparedStatement.setString(counter++, String.join("\n", arr));
                            break;
                        case VARCHAR:
                            String s;
                            if (declaredField.getType() == Material.class)
                                s = ((Material) declaredField.get(configuration)).name();
                            else
                                s = (String) declaredField.get(configuration);
                            preparedStatement.setString(counter++, s);
                            break;
                    }
                    declaredField.setAccessible(false);
                }
            } catch (SQLException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return counter;
    }

    @Override
    public void saveData(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                executeSaveData(configuration);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void saveDataSync(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        executeSaveData(configuration);
    }

    @Override
    public <T extends DatabaseConfiguration> void saveData(String searchKey, String searchValue, T configuration, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                executeSaveData(configuration);
                callback.onQueryDone(configuration, null);
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @SuppressWarnings("unchecked")
    private <T extends DatabaseConfiguration> void executeSaveData(T configuration) {
        if (configuration.isUpdated()) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(
                        prepareSaveQuery(configuration), Statement.RETURN_GENERATED_KEYS);

                fillAllFields(preparedStatement, configuration, 1);
                if (preparedStatement.executeUpdate() == 0) return;

                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next())
                        configuration.setId(generatedKeys.getInt(1));
                }

                Log.info(configuration.getClass().getName());
                List<Field> fields = ReflectionHelper.getAllFields(new ArrayList<>(), configuration.getClass());

                for (Field field : fields)
                    if (field.isAnnotationPresent(MySQLJoin.class)) {
                        MySQLJoin annotation = field.getAnnotation(MySQLJoin.class);
                        field.setAccessible(true);
                        ArrayList<DatabaseConfiguration> data = (ArrayList<DatabaseConfiguration>) field.get(configuration);
                        preparedStatement = connection.prepareStatement(prepareSaveQuery(data, annotation.object()));

                        int counter = 1;
                        for (DatabaseConfiguration d : data)
                            counter = fillAllFields(preparedStatement, d, counter);

                        Log.info(preparedStatement);
                        preparedStatement.executeUpdate();

                        field.setAccessible(false);
                    }


                configuration.setUpdated(false);
            } catch (SQLException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteData(String searchKey, String searchValue, DatabaseConfiguration configuration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    String sql = "DELETE FROM " + getTableName(configuration.getClass()) + " WHERE " + searchKey + " = ?;";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setString(1, searchValue);
                    Log.info(preparedStatement);
                    preparedStatement.execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public void clearAllData(Class<? extends DatabaseConfiguration> c) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    String sql = "TRUNCATE TABLE " + getTableName(c) + ";";
                    Log.info(sql);
                    connection.prepareStatement(sql).execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));
    }

    @Override
    public <T extends DatabaseConfiguration> void loadAll(Class<T> c, LoadDataCallback<T> callback) {
        loadAll(null, null, c, callback);
    }

    @Override
    public <T extends DatabaseConfiguration> void loadAll(String searchKey, Object searchValue, Class<T> c, LoadDataCallback<T> callback) {
        new BukkitRunnable() {
            @Override
            public void run() {
                ArrayList<T> output = new ArrayList<>();
                String sql = "SELECT * FROM " + getTableName(c);
                if (searchKey != null) sql += " WHERE " + searchKey + " = ?;";
                else sql += ";";
                try {
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    if (searchKey != null) {
                        if (searchValue instanceof String)
                            preparedStatement.setString(1, (String) searchValue);
                        else if (searchValue instanceof Integer)
                            preparedStatement.setInt(1, (Integer) searchValue);
                        else if (searchValue instanceof Long)
                            preparedStatement.setLong(1, (Long) searchValue);
                        else if (searchValue instanceof Double)
                            preparedStatement.setDouble(1, (Double) searchValue);
                        else if (searchValue instanceof Boolean)
                            preparedStatement.setBoolean(1, (Boolean) searchValue);
                        else
                            preparedStatement.setObject(1, searchValue);
                    }
                    Log.info(preparedStatement);
                    ResultSet result = preparedStatement.executeQuery();

                    while (result.next()) {
                        T databaseConfiguration = c.newInstance().mapFieldsFromMySQL(result, (result1, results) -> {
                            callback.onQueryDone(null, output);
                        });
                        StringFormatter.formatAllFields(databaseConfiguration);
                        output.add(databaseConfiguration);
                    }

                } catch (SQLException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.getPlugin(Main.class));

    }

    private <T extends DatabaseConfiguration> String getTableName(Class<T> configuration) {
        return configuration.getAnnotation(MySQLTable.class).value();
    }

    public boolean isConnected() {
        return isConnected;
    }
}
