package com.divictusgaming.dailyShop.database;

import java.util.List;

public interface IDatabase {

    <T extends DatabaseConfiguration> void loadData(String searchKey, String searchValue,
                                                    Class<T> outputClass,
                                                    LoadDataCallback<T> callback);

    void saveData(String searchKey, String searchValue, DatabaseConfiguration configuration);

    void saveDataSync(String searchKey, String searchValue, DatabaseConfiguration configuration);

    <T extends DatabaseConfiguration> void saveData(String searchKey, String searchValue, T configuration,
                                                    LoadDataCallback<T> callback);

    void deleteData(String searchKey, String searchValue, DatabaseConfiguration configuration);

    void clearAllData(Class<? extends DatabaseConfiguration> c);

    <T extends DatabaseConfiguration> void loadAll(Class<T> c, LoadDataCallback<T> callback);

    <T extends DatabaseConfiguration> void loadAll(String searchKey, Object searchValue, Class<T> c, LoadDataCallback<T> callback);

    boolean isConnected();

    interface LoadDataCallback<T extends DatabaseConfiguration> {
        void onQueryDone(T result, List<T> results);
        default void onError(Exception e){

        }
    }
}
