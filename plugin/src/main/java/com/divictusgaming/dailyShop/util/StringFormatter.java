package com.divictusgaming.dailyShop.util;

import com.divictusgaming.dailyShop.database.DatabaseConfiguration;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFormatter {
    private static final Pattern HEX_PATTERN = Pattern.compile("<(#[a-fA-F0-9]{6})>");
    private static final Pattern HOVER_PATTERN = Pattern.compile("\\[(command|suggest|hover):(.+?)]");

    public static String format(String message) {
        if (message == null || message.isEmpty()) return message;
        Matcher matcher = HEX_PATTERN.matcher(message);

        while (matcher.find())
            message = message.replace(matcher.group(0),
                    net.md_5.bungee.api.ChatColor.of(matcher.group(1)).toString());

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static void sendChatMessage(Player p, String message) {
        Matcher matcher = HOVER_PATTERN.matcher(message);
        boolean found = false;
        TextComponent msg = new TextComponent();
        while (matcher.find()) {
            switch (matcher.group(1)) {
                case "command":
                    msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, matcher.group(2)));
                    break;
                case "suggest":
                    msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, matcher.group(2)));
                    break;
                case "hover":
                    try {
                        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                new Text(String.join("\n", matcher.group(2).split("\\\\n")))));
                    } catch (NoClassDefFoundError e) {
                        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                new BaseComponent[]{new TextComponent(String.join("\n", matcher.group(2).split("\\\\n")))}));
                    }
                    break;
            }
            message = message.replace(matcher.group(0), "");
            found = true;
        }
        msg.setText(message);

        if (found)
            p.spigot().sendMessage(msg);
        else
            p.sendMessage(message);

    }

    public static void format(List<String> messages) {
        for (int i = 0; i < messages.size(); i++)
            messages.set(i, StringFormatter.format(messages.get(i)));

    }

    //change the formatting color of all fields
    @SuppressWarnings("unchecked")
    public static void formatAllFields(Object object) {
        if (object == null) return;
        if (!(object instanceof DatabaseConfiguration)) return;

        for (Field declaredField : ReflectionHelper.getAllFields(new ArrayList<>(), object.getClass())) {
            try {
                declaredField.setAccessible(true);
                if (declaredField.getType() == String.class) {
                    declaredField.set(object, format((String) declaredField.get(object)));
                } else if (declaredField.getType() == List.class || List.class.isAssignableFrom(declaredField.getType())) {

                    List<Object> o = (List<Object>) declaredField.get(object);
                    if (o != null)
                        for (int i = 0; i < o.size(); i++)
                            if (o.get(i) instanceof String) o.set(i, format((String) o.get(i)));
                            else formatAllFields(o.get(i));

                } else formatAllFields(declaredField.get(object));

                declaredField.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    //method for extracting locations from string
    public static Location stringToLocation(String str) {
        String[] split = str.split(";");
        if (split.length > 4) {
            return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]),
                    Float.parseFloat(split[4]), Float.parseFloat(split[5]));
        }
        return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));
    }

    //Make location to string
    public static String locationToString(Location loc, boolean saveYawAndPitch) {
        if (saveYawAndPitch)
            return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch();
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
    }

    public static String replacePlaceholders(Player player, String str) {
        str = PlaceholderReplacer.setPlaceholders(player, str);
        return PlaceholderAPI.setPlaceholders(player, str);
    }

    public static String replacePlaceholders(Player player, String str, HashMap<String, String> additionalPlaceholders) {
        if (additionalPlaceholders != null) str = PlaceholderReplacer.setPlaceholders(str, additionalPlaceholders);
        if (player == null) return str;
        return replacePlaceholders(player, str);
    }

    public static List<String> replacePlaceholders(Player player, List<String> str) {
        PlaceholderReplacer.setPlaceholders(player, str);
        return PlaceholderAPI.setPlaceholders(player, str);
    }

    public static List<String> replacePlaceholders(Player player, List<String> str, HashMap<String, String> additionalPlaceholders) {
        if (additionalPlaceholders != null) PlaceholderReplacer.setPlaceholders(str, additionalPlaceholders);
        if (player == null) return str;
        return replacePlaceholders(player, str);
    }

}
