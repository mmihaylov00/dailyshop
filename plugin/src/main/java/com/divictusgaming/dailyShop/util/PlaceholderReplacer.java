package com.divictusgaming.dailyShop.util;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.sub.CurrencyYAMLConfiguration;
import com.divictusgaming.dailyShop.exception.NotInWizardException;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaceholderReplacer {
    private static final Pattern PAPI_PATTERN = Pattern.compile("%dailyshop_([a-zA-Z0-9_]+)%");
    private static final Pattern COMMAND_PATTERN = Pattern.compile("\\{([a-zA-Z0-9_]+)}");
    private static final Pattern IF_PATTERN = Pattern.compile("\\{if:([a-zA-Z0-9_]+)}");

    public static String setPlaceholders(Player p, String text) {
        text = setPlaceholders(p, text, PAPI_PATTERN);
        text = setPlaceholders(p, text, COMMAND_PATTERN);
        return text;
    }

    private static String setPlaceholders(Player player, String text, Pattern pattern) {
        final Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            try {
                Placeholder placeholder = Placeholder.valueOf(matcher.group(1).toUpperCase(Locale.ROOT));
                text = text.replace(matcher.group(0), placeholder.getValue(player));
            } catch (IllegalArgumentException ignored) {
            }
        }
        return text;
    }

    public static String setPlaceholders(String text, HashMap<String, String> placeholders) {
        Matcher matcher = IF_PATTERN.matcher(text);
        while (matcher.find()) {
            try {
                if (placeholders.get(matcher.group(1)) == null || Integer.parseInt(placeholders.get(matcher.group(1))) == 0)
                    text = "";
                else
                    text = text.replace(matcher.group(0), "");
            } catch (NumberFormatException e) {
                text = "";
            }
        }

        if (!text.isEmpty()) {
            matcher = COMMAND_PATTERN.matcher(text);
            while (matcher.find())
                text = text.replace(matcher.group(0), placeholders.get(matcher.group(1)) + "");
        }
        return text;
    }

    public static void setPlaceholders(List<String> text, HashMap<String, String> placeholders) {
        for (int i = 0; i < text.size(); i++) {
            String s = setPlaceholders(text.get(i), placeholders);
            if (s.isEmpty()) text.remove(i--);
            else text.set(i, s);
        }
    }

    public static void setPlaceholders(Player p, List<String> text) {
        StringJoiner joiner = new StringJoiner("\\n");
        for (String s : text) {
            String setPlaceholders = setPlaceholders(p, s);
            joiner.add(setPlaceholders);
        }
        String t = joiner.toString();
        //add the new lines from a placeholder to the list
        text.clear();
        text.addAll(Arrays.asList(t.split("\\\\n")));

        //attach the colors if the placeholder added new lines
        String lastLineColors = null;
        for (int i = 0; i < text.size(); i++) {
            String line = text.get(i);
            if (lastLineColors != null && line.charAt(0) != ChatColor.COLOR_CHAR) {
                text.set(i, lastLineColors + line);
                continue;
            }
            lastLineColors = ChatColor.getLastColors(line);
        }
    }

    public enum Placeholder {
        REFRESH_TIME((p) -> {
            return Main.getPlugin(Main.class).getTimer().getNextRefreshAsString();
        }),
        REFRESH_COST_CATEGORY((p) -> {
            Main plugin = Main.getPlugin(Main.class);
            CurrencyYAMLConfiguration currency = plugin.getConfiguration().getRefreshCurrency();
            String amount = String.format(currency.getRounded() ? "%.0f" : "%.2f",
                    plugin.getConfiguration().getRefreshCategoryCost());
            return ChatColor.getLastColors(currency.getDisplayName()) + amount + " " + currency.getDisplayName();
        }),
        REFRESH_COST((p) -> {
            Main plugin = Main.getPlugin(Main.class);
            CurrencyYAMLConfiguration currency = plugin.getConfiguration().getRefreshCurrency();
            String amount = String.format(currency.getRounded() ? "%.0f" : "%.2f",
                    plugin.getConfiguration().getRefreshCategoryCost() * plugin.getCategoryStorage().getCategoriesCount());
            return ChatColor.getLastColors(currency.getDisplayName()) + amount + " " + currency.getDisplayName();
        }),
        EDIT_CHANCE((p) -> {
            try {
                Main plugin = Main.getPlugin(Main.class);
                Double percentage = plugin.getItemCreationWizard().getItem(p).getPercentage();
                return percentage == null ? plugin.getMessages().getNotSet() : percentage + "%";
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_COMMAND((p) -> {
            try {
                Main plugin = Main.getPlugin(Main.class);
                String command = plugin.getItemCreationWizard().getItem(p).getCommand();
                return command == null ? plugin.getMessages().getNotSet() : command;
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_HIDE_TAGS((p) -> {
            try {
                return "" + Main.getPlugin(Main.class).getItemCreationWizard().getItem(p).isHideTags();
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_CONDITIONS((p) -> {
            try {
                return Main.getPlugin(Main.class).getItemCreationWizard().getItem(p).getConditionsString();
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_VARIABLES((p) -> {
            try {
                return Main.getPlugin(Main.class).getItemCreationWizard().getItem(p).getVariablesString();
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_LORE((p) -> {
            try {
                return Main.getPlugin(Main.class).getItemCreationWizard().getItem(p).getLoreString();
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        EDIT_PRICES((p) -> {
            try {
                return Main.getPlugin(Main.class).getItemCreationWizard().getItem(p).getPricesString();
            } catch (NotInWizardException e) {
                return "";
            }
        }),
        PLAYER(HumanEntity::getName),
        ;

        private interface Callback {
            String getValue(Player player);
        }

        private final Callback callback;

        Placeholder(Callback callback) {
            this.callback = callback;
        }

        public String getValue(Player p) {
            return callback.getValue(p);
        }
    }
}
