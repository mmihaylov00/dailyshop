package com.divictusgaming.dailyShop.util;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.MessagesYAMLConfiguration;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

public class RefreshTimer {
    private long nextRefresh;

    public RefreshTimer() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        long startOfToday = today.getTimeInMillis();

        int refreshAfter = Main.getPlugin(Main.class).getConfiguration().getRefreshAfter() * 60 * 1000;
        while (startOfToday < new Date().getTime())
            startOfToday += refreshAfter;

        nextRefresh = startOfToday;
    }

    public long getNextRefresh() {
        return nextRefresh;
    }

    public void testRefresh() {
        if (nextRefresh <= new Date().getTime()) {
            Main pl = Main.getPlugin(Main.class);
            nextRefresh += pl.getConfiguration().getRefreshAfter();
            pl.getPlayerDataStorage().refreshAllPlayers();
        }
    }

    public String getNextRefreshAsString() {
        MessagesYAMLConfiguration messages = Main.getPlugin(Main.class).getMessages();
        Duration duration = Duration.ofMillis(getUntilNextRefresh());
        long[] times = new long[3];
        times[0] = duration.toHours();
        duration = duration.minusHours(times[0]);
        times[1] = duration.toMinutes();
        duration = duration.minusMinutes(times[1]);
        times[2] = duration.getSeconds();

        StringBuilder sb = new StringBuilder();

        if (times[0] > 0) {
            append(sb, times[0], messages.getHour(), messages.getHours());
            if (times[1] > 0) append(sb, times[1], messages.getMinute(), messages.getMinutes());
        } else if (times[1] > 0) {
            append(sb, times[1], messages.getMinute(), messages.getMinutes());
            if (times[2] > 0) append(sb, times[2], messages.getSecond(), messages.getSeconds());
        } else {
            append(sb, times[2], messages.getSecond(), messages.getSeconds());
        }
        return sb.toString();
    }

    private void append(StringBuilder sb, long time, String single, String plural) {
        if (time > 0) {
            sb.append(time);
            if (time > 1) sb.append(plural);
            else sb.append(single);
        }
    }

    public long getUntilNextRefresh() {
        return nextRefresh - new Date().getTime();
    }
}
