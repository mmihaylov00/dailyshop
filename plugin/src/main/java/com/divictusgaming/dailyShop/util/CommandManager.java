package com.divictusgaming.dailyShop.util;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.exception.ExistingCommandCategoryException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;

import java.util.HashSet;

public class CommandManager {
    private final HashSet<String> registeredCommands;
    private final CommandMap commandMap;

    public CommandManager(Main plugin) {
        this.registeredCommands = new HashSet<>();
        this.commandMap = plugin.getVersionMain().getCommandMap(plugin);
    }

    public void clearCommands(){
        for (String registeredCommand : registeredCommands) {
            Command command = commandMap.getCommand(registeredCommand);
            if (command != null) command.unregister(commandMap);
        }
        registeredCommands.clear();
    }

    public void registerCommand(String commandName, BukkitCommand command) throws ExistingCommandCategoryException {
        if (commandName.charAt(0) == '/') commandName = commandName.substring(1);
        if (!registeredCommands.add(commandName)) throw new ExistingCommandCategoryException();
        commandMap.register(commandName, command);
    }

    public void unregisterCommand(String commandName){
        if (commandName.charAt(0) == '/') commandName = commandName.substring(1);
        if (!registeredCommands.remove(commandName)) return;
        Command command = commandMap.getCommand(commandName);
        if (command != null) command.unregister(commandMap);
    }
}
