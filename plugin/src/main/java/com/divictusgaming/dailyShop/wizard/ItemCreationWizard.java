package com.divictusgaming.dailyShop.wizard;

import com.divictusgaming.dailyShop.Main;
import com.divictusgaming.dailyShop.database.configuration.CategoryConfiguration;
import com.divictusgaming.dailyShop.database.configuration.ShopItemConfiguration;
import com.divictusgaming.dailyShop.exception.*;
import com.divictusgaming.dailyShop.gui.menu.ItemEditorMenu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ItemCreationWizard {
    private final HashMap<UUID, ShopItemConfiguration> configurations;

    public ItemCreationWizard() {
        this.configurations = new HashMap<>();
    }

    public void create(Player p, String name, String category) throws AirInHandException, CategoryNotFoundException, ExistingItemException, AlreadyInWizardException {
        ItemStack item = p.getInventory().getItemInHand();
        if (item.getType() == Material.AIR)
            throw new AirInHandException();

        if (configurations.containsKey(p.getUniqueId()))
            throw new AlreadyInWizardException();

        if (configurations.values().stream().anyMatch(c -> c.getName().equals(name)))
            throw new ExistingItemException();

        CategoryConfiguration categoryConfiguration = Main.getPlugin(Main.class).getCategoryStorage().getCategory(category);
        if (categoryConfiguration.getShopItems().stream().anyMatch(c -> c.getName().equals(name)))
            throw new ExistingItemException();

        ShopItemConfiguration shopItemConfiguration = new ShopItemConfiguration(item, name, category);
        configurations.put(p.getUniqueId(), shopItemConfiguration);
        new ItemEditorMenu(p);
    }

    public void addToEdit(Player p, ShopItemConfiguration c) {
        configurations.put(p.getUniqueId(), c);
    }

    public void deleteCategory(String categoryName) {
        for (Map.Entry<UUID, ShopItemConfiguration> entry : configurations.entrySet())
            if (entry.getValue().getCategory().equals(categoryName))
                configurations.remove(entry.getKey());
    }

    public void save(Player p) throws NotInWizardException, EmptyConfigurationException, ConfigurationNotSetException {
        ShopItemConfiguration configuration = this.configurations.get(p.getUniqueId());

        if (configuration == null) throw new NotInWizardException();
        if (configuration.getCommand() == null || configuration.getCommand().isEmpty())
            throw new ConfigurationNotSetException("command");
        if (configuration.getPercentage() == null) throw new ConfigurationNotSetException("chance");
        if (configuration.getConditions().isEmpty()) throw new EmptyConfigurationException("condition");
        if (configuration.getPrices().isEmpty()) throw new EmptyConfigurationException("price");

        this.configurations.remove(p.getUniqueId());
        try {
            Main.getPlugin(Main.class).getCategoryStorage().addItem(configuration.getCategory(), configuration);
        } catch (CategoryNotFoundException ignored) { //can't happen
        }
    }

    public void cancel(Player p) throws NotInWizardException {
        if (this.configurations.remove(p.getUniqueId()) == null)
            throw new NotInWizardException();
    }

    public ShopItemConfiguration getItem(Player p) throws NotInWizardException {
        ShopItemConfiguration configuration = configurations.get(p.getUniqueId());
        if (configuration == null) throw new NotInWizardException();
        return configuration;
    }
}
